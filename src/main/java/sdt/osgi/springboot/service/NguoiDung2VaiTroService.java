package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.NguoiDung2VaiTroRepository;
import sdt.osgi.springboot.dto.NguoiDung2VaiTroDTO;
import sdt.osgi.springboot.mapper.NguoiDung2VaiTroMapper;
import sdt.osgi.springboot.model.NguoiDung2VaiTro;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class NguoiDung2VaiTroService {

    private final NguoiDung2VaiTroRepository nguoiDung2VaiTroRepository;
    private final NguoiDung2VaiTroMapper nguoiDung2VaiTroMapper;

    public List<NguoiDung2VaiTroDTO> getListNguoiDung2VaiTro() {

        return nguoiDung2VaiTroMapper.toNguoiDung2VaiTroDTOList(nguoiDung2VaiTroRepository.findAll());

    }

    public List<NguoiDung2VaiTro> findTop1000() {

        return nguoiDung2VaiTroRepository.findTop1000By();
    }

    public void add(NguoiDung2VaiTroDTO nguoiDung2VaiTroDTO) {

        NguoiDung2VaiTro nguoiDung2VaiTro = nguoiDung2VaiTroMapper.nguoiDung2VaiTroDTOToNguoiDung2VaiTro(nguoiDung2VaiTroDTO);

        nguoiDung2VaiTroRepository.save(nguoiDung2VaiTro);
    }

    public void update(NguoiDung2VaiTroDTO nguoiDung2VaiTroDTO) {
        NguoiDung2VaiTro nguoiDung2VaiTroOrigin = nguoiDung2VaiTroRepository.findById(nguoiDung2VaiTroDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay nguoi dung 2 vai tro!!!"));
        NguoiDung2VaiTro nguoiDung2VaiTro = NguoiDung2VaiTro.builder()
                .id(nguoiDung2VaiTroDTO.getId())
                .vaiTro(nguoiDung2VaiTroDTO.getVaiTro() != null ? nguoiDung2VaiTroDTO.getVaiTro() : nguoiDung2VaiTroOrigin.getVaiTro())
                .congChuc(nguoiDung2VaiTroDTO.getCongChuc() != null ? nguoiDung2VaiTroDTO.getCongChuc() : nguoiDung2VaiTroOrigin.getCongChuc()).build();
        nguoiDung2VaiTroRepository.save(nguoiDung2VaiTro);
    }

    public NguoiDung2VaiTroDTO getNguoiDung2VaiTro(long nguoiDung2VaiTroId) {

        Optional<NguoiDung2VaiTro> nguoiDung2VaiTro = nguoiDung2VaiTroRepository.findById(nguoiDung2VaiTroId);

        return nguoiDung2VaiTroMapper.nguoiDung2VaiTroToNguoiDung2VaiTroDTO(nguoiDung2VaiTro.get());
    }

    public void delete(long nguoiDung2VaiTroId) {

        nguoiDung2VaiTroRepository.deleteById(nguoiDung2VaiTroId);
    }

    public void deleteByCongChucIdAndVaiTroId(long congChucId, long vaiTroId) {

        nguoiDung2VaiTroRepository.deleteByCongChuc_IdAndVaiTro_Id(congChucId, vaiTroId);
    }

    public List<NguoiDung2VaiTroDTO> findByNguoiDung2VaiTroIn(List<Long> ids) {

        return nguoiDung2VaiTroMapper.nguoiDung2VaiTrosToNguoiDung2VaiTroDTOs(nguoiDung2VaiTroRepository.findByIdIn(ids));

    }

    public List<NguoiDung2VaiTroDTO> findAllByCongChucId(Long congChucId) {

        return nguoiDung2VaiTroMapper.nguoiDung2VaiTrosToNguoiDung2VaiTroDTOs(nguoiDung2VaiTroRepository.findAllByCongChucId(congChucId));

    }

    public List<NguoiDung2VaiTroDTO> saveListNguoiDung2VaiTroDTO(List<NguoiDung2VaiTroDTO> nguoiDung2VaiTroDTOS) {
        List<NguoiDung2VaiTro> listNguoiDung2VaiTros = nguoiDung2VaiTroMapper.toNguoiDung2VaiTroList(nguoiDung2VaiTroDTOS);
        return nguoiDung2VaiTroMapper.toNguoiDung2VaiTroDTOList(nguoiDung2VaiTroRepository.saveAll(listNguoiDung2VaiTros));
    }
}
