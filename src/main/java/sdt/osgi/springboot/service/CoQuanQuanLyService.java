package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.CapCoQuanQuanLyRepository;
import sdt.osgi.springboot.dao.CoQuanQuanLyRepository;
import sdt.osgi.springboot.dto.CapCoQuanQuanLyDTO;
import sdt.osgi.springboot.dto.CoQuanQuanLyDTO;
import sdt.osgi.springboot.mapper.CapCoQuanQuanLyMapper;
import sdt.osgi.springboot.mapper.CoQuanQuanLyMapper;
import sdt.osgi.springboot.model.CapCoQuanQuanLy;
import sdt.osgi.springboot.model.CoQuanQuanLy;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class CoQuanQuanLyService {

    private final CoQuanQuanLyRepository coQuanQuanLyRepository;
    private final CoQuanQuanLyMapper coQuanQuanLyMapper;

    private final CapCoQuanQuanLyRepository capCoQuanQuanLyRepository;
    private final CapCoQuanQuanLyMapper capCoQuanQuanLyMapper;

    public CoQuanQuanLyDTO getCoQuanQuanLy(long coQuanQuanLyId) {
        if (coQuanQuanLyId > 0) {
            return coQuanQuanLyMapper.toCoQuanQuanLyDTO(coQuanQuanLyRepository.findById(coQuanQuanLyId).get());
        } else {
            return coQuanQuanLyMapper.toCoQuanQuanLyDTO(coQuanQuanLyRepository.findFirstByIdAndTrangThai(coQuanQuanLyId, Constants.TrangThaiHoatDong.MO_KHOA));
        }
    }

    public List<CoQuanQuanLyDTO> getListCoQuanQuanLy() {
        return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findByDaKhoaAndTrangThai(Constants.TAO_MOI, Constants.TrangThaiHoatDong.MO_KHOA));
    }

    public List<CoQuanQuanLyDTO> getListCoQuanQuanLyBySearch(String keyword) {
        return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findAllByTenContainingOrMaContainingAndDaKhoaAndTrangThai(keyword, keyword, Constants.TAO_MOI, Constants.TrangThaiHoatDong.MO_KHOA));
    }

    public List<CoQuanQuanLyDTO> getListCoQuanQuanLyByParentId(long parentId) {
        if (parentId > 0) {
            return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findByChaIdAndTrangThai(parentId, Constants.TrangThaiHoatDong.MO_KHOA));
        } else {
            return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findByDaKhoaAndTrangThai(Constants.TAO_MOI, Constants.TrangThaiHoatDong.MO_KHOA));
        }
    }

    public List<CoQuanQuanLyDTO> getListCoQuanQuanLyByParentIdBySearch(long parentId, String keyword) {
        return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findAllByTenContainingOrMaContainingAndChaIdAndDaKhoaAndTrangThai(keyword, keyword, parentId, Constants.TAO_MOI, Constants.TrangThaiHoatDong.MO_KHOA));
    }

    public void add(CoQuanQuanLyDTO coQuanQuanLyDTO) {

        CoQuanQuanLy coQuanQuanLy = coQuanQuanLyMapper.toCoQuanQuanLy(coQuanQuanLyDTO);
        coQuanQuanLy.setDaKhoa(Constants.TAO_MOI);
        coQuanQuanLyRepository.save(coQuanQuanLy);

    }

    public void update(CoQuanQuanLyDTO coQuanQuanLyDTO) {
        CoQuanQuanLy coQuanQuanLyOrigin = coQuanQuanLyRepository.findById(coQuanQuanLyDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay co quan quan ly!!!"));

        CoQuanQuanLy coQuanQuanLy = CoQuanQuanLy.builder()
                .id(coQuanQuanLyDTO.getId())
                .ma(coQuanQuanLyDTO.getMa() != null ? coQuanQuanLyDTO.getMa() : coQuanQuanLyOrigin.getMa())
                .diaChi(coQuanQuanLyDTO.getDiaChi() != null ? coQuanQuanLyDTO.getDiaChi() : coQuanQuanLyOrigin.getDiaChi())
                .ten(coQuanQuanLyDTO.getTen() != null ? coQuanQuanLyDTO.getTen() : coQuanQuanLyOrigin.getTen())
                .capCoQuanQuanLy(coQuanQuanLyDTO.getCapCoQuanQuanLy() != null ? coQuanQuanLyMapper.toCoQuanQuanLy(coQuanQuanLyDTO).getCapCoQuanQuanLy() : coQuanQuanLyOrigin.getCapCoQuanQuanLy())
                .chaId(coQuanQuanLyDTO.getChaId())
                .daKhoa(Constants.TAO_MOI).build();

        coQuanQuanLy.setNgayTao(coQuanQuanLyOrigin.getNgayTao());
        coQuanQuanLyRepository.save(coQuanQuanLy);
    }

    public void delete(long coQuanQuanLyId) {

        CoQuanQuanLy coQuanQuanLy = coQuanQuanLyRepository.findById(coQuanQuanLyId).get();
        coQuanQuanLy.setDaKhoa(Constants.DA_XOA);
        coQuanQuanLyRepository.save(coQuanQuanLy);
    }

    public List<CoQuanQuanLyDTO> getListCoQuanQuanLyByDsCoQuanQuanLyId(String dsCoQuanQuanLyId) {
        return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findByIdInAndTrangThai(Stream.of(dsCoQuanQuanLyId.split(","))
                .map(Long::valueOf).collect(Collectors.toList()), Constants.TrangThaiHoatDong.MO_KHOA));
    }

    public CoQuanQuanLyDTO findFirstByMa(String ma) {

        return coQuanQuanLyMapper.toCoQuanQuanLyDTO(coQuanQuanLyRepository.findFirstByMaAndDaKhoaAndTrangThai(ma, Constants.TAO_MOI, Constants.TrangThaiHoatDong.MO_KHOA));
    }

    public Page<CoQuanQuanLy> getListCoQuanQuanLyAndPaginate(String searchTxt, int page, int size, long chaId) {

        Pageable pageable = PageRequest.of(page, size);
        if (chaId > 0) {
            if (searchTxt != null && searchTxt.trim().length() > 0) {
                return coQuanQuanLyRepository
                        .findByMaContainingAndChaIdOrTenContainingAndChaIdAndDaKhoa(searchTxt, chaId, searchTxt, chaId, Constants.TAO_MOI, pageable);
            } else {
                return coQuanQuanLyRepository.findByChaIdAndDaKhoa(chaId, Constants.TAO_MOI, pageable);
            }
        } else {
            if (searchTxt != null && searchTxt.trim().length() > 0) {
                return coQuanQuanLyRepository.findByMaContainingOrTenContainingAndDaKhoa(searchTxt, searchTxt, Constants.TAO_MOI, pageable);
            } else {
                return coQuanQuanLyRepository.findAllByDaKhoa(Constants.TAO_MOI, pageable);
            }
        }
    }

    public Page<CoQuanQuanLy> getListCoQuanQuanLyAndPaginateByDonVi(String searchTxt, int page, int size, long chaId) {

        Pageable pageable = PageRequest.of(page, size);
        if (chaId > 0) {
            if (searchTxt != null && searchTxt.trim().length() > 0) {
                return coQuanQuanLyRepository
                        .findByDiaChiContainingAndChaIdAndDaKhoa(searchTxt, chaId, Constants.TAO_MOI, pageable);
            } else {
                return coQuanQuanLyRepository.findByChaIdAndDaKhoa(chaId, Constants.TAO_MOI, pageable);
            }
        } else {
            if (searchTxt != null && searchTxt.trim().length() > 0) {
                return coQuanQuanLyRepository.findByDiaChiContainingAndDaKhoa(searchTxt, Constants.TAO_MOI, pageable);
            } else {
                return coQuanQuanLyRepository.findAllByDaKhoa(Constants.TAO_MOI, pageable);
            }
        }
    }

    public List<CoQuanQuanLyDTO> getListCoQuanQuanLyByParentIdAndCap(long parentId, String capCoquans) {
        List<Long> listCapCoQuan = StringUtils.convertStringToLongs(capCoquans);
        return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findByChaIdAndCapCoQuanQuanLyIdInAndTrangThai(parentId, listCapCoQuan, Constants.TrangThaiHoatDong.MO_KHOA));
    }

    public List<CoQuanQuanLyDTO> findByChaIdInAndCapCoQuanQuanLyIdIn(String parentIds, String capCoquans) {
        List<Long> listCapCoQuan = StringUtils.convertStringToLongs(capCoquans);
        List<Long> listCoQuanId = StringUtils.convertStringToLongs(parentIds);
        return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findByChaIdInAndCapCoQuanQuanLyIdInAndTrangThai(listCoQuanId, listCapCoQuan, Constants.TrangThaiHoatDong.MO_KHOA));
    }

    public List<CapCoQuanQuanLyDTO> getAllCapCoQuanQuanLy() {
        return capCoQuanQuanLyMapper.toCapCQQLDTOList(capCoQuanQuanLyRepository.findAll());
    }

    public void khoaCoQuanQuanLy(long coQuanQuanLyId, int trangThai) {

        CoQuanQuanLy coQuanQuanLy = coQuanQuanLyRepository.findById(coQuanQuanLyId).orElseThrow(() -> new EntityNotFoundException(Constants.KHONG_TIM_THAY + coQuanQuanLyId));
        coQuanQuanLy.setTrangThai(trangThai);
        coQuanQuanLyRepository.save(coQuanQuanLy);
    }

    public List<CoQuanQuanLyDTO> findAllByChaIdIn(String chaIdStr) {
        List<Long> chaIds = Stream.of(chaIdStr.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());
        return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findAllByChaIdIn(chaIds));
    }

    public List<CoQuanQuanLyDTO> findAllByIdIn(String IdStr) {
        List<Long> chaIds = Stream.of(IdStr.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());
        return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findAllByIdIn(chaIds));
    }

    public List<CoQuanQuanLyDTO> findByCapId (Long capId) {
        return coQuanQuanLyMapper.toCQQLDTOList(coQuanQuanLyRepository.findByCapCoQuanQuanLy_Id(capId));
    }

}
