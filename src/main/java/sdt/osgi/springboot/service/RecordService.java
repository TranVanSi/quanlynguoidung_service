package sdt.osgi.springboot.service;

import sdt.osgi.springboot.dao.RecordsRepository;
import sdt.osgi.springboot.dto.RecordDTO;
import sdt.osgi.springboot.mapper.RecordMapper;
import sdt.osgi.springboot.model.Record;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RecordService {

    private final RecordsRepository recordsRepository;
    private final RecordMapper recordMapper;

    public Iterable<Record> getRecords() {
        return recordsRepository.findAll();
    }

    public void addRecord(RecordDTO recordDTO) {
        Record record = recordMapper.recordDTOToRecord(recordDTO);
        recordsRepository.save(record);
    }
}
