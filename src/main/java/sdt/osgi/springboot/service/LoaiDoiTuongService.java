package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.LoaiDoiTuongRepository;
import sdt.osgi.springboot.dto.LoaiDoiTuongDTO;
import sdt.osgi.springboot.mapper.LoaiDoiTuongMapper;
import sdt.osgi.springboot.model.LoaiDoiTuong;

import java.util.List;

@Component
@RequiredArgsConstructor
public class LoaiDoiTuongService {

    private final LoaiDoiTuongRepository loaiDoiTuongRepository;
    private final LoaiDoiTuongMapper loaiDoiTuongMapper;

    public List<LoaiDoiTuong> findTop1000(){

        return loaiDoiTuongRepository.findTop1000By();
    }

    public void add(LoaiDoiTuongDTO loaiDoiTuongDTO) {

        LoaiDoiTuong loaiDoiTuong = loaiDoiTuongMapper.loaiDoiTuongDTOToLoaiDoiTuong(loaiDoiTuongDTO);

        loaiDoiTuongRepository.save(loaiDoiTuong);
    }

    public void update(LoaiDoiTuongDTO loaiDoiTuongDTO) {
        LoaiDoiTuong loaiDoiTuongOrigin = loaiDoiTuongRepository.findById(loaiDoiTuongDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        LoaiDoiTuong loaiDoiTuong = LoaiDoiTuong.builder()
                .id(loaiDoiTuongDTO.getId())
                .ma(loaiDoiTuongDTO.getMa() != null ? loaiDoiTuongDTO.getMa() : loaiDoiTuongOrigin.getMa())
                .moTa(loaiDoiTuongDTO.getMoTa() != null ? loaiDoiTuongDTO.getMoTa() : loaiDoiTuongOrigin.getMoTa())
                .ten(loaiDoiTuongDTO.getTen() != null ? loaiDoiTuongDTO.getTen() : loaiDoiTuongOrigin.getTen())
                .trangThai(loaiDoiTuongDTO.getTrangThai() != null ? loaiDoiTuongDTO.getTrangThai() : loaiDoiTuongOrigin.getTrangThai()).build();

        loaiDoiTuongRepository.save(loaiDoiTuong);
    }
}
