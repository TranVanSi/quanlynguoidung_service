package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Component;
import sdt.osgi.springboot.dao.ChucVu2CoQuan2VaiTroRepository;
import sdt.osgi.springboot.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.osgi.springboot.dto.ChucVu2VaiTroDTO;
import sdt.osgi.springboot.dto.ChucVuDTO;
import sdt.osgi.springboot.dto.CoQuanQuanLyDTO;
import sdt.osgi.springboot.mapper.ChucVu2CoQuan2VaiTroMapper;
import sdt.osgi.springboot.mapper.ChucVuMapper;
import sdt.osgi.springboot.model.ChucVu;
import sdt.osgi.springboot.model.ChucVu2CoQuan2VaiTro;
import sdt.osgi.springboot.model.TaiNguyen;
import sdt.osgi.springboot.model.VaiTro;
import sdt.osgi.springboot.util.DatatableUtil;
import sdt.osgi.springboot.util.StringUtils;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Transactional
public class ChucVu2CoQuan2VaiTroService {

    private final ChucVu2CoQuan2VaiTroRepository chucVu2CoQuan2VaiTroRepository;
    private final ChucVu2CoQuan2VaiTroMapper chucVu2CoQuan2VaiTroMapper;
    private final ChucVuMapper chucVuMapper;
    private final ChucVuService chucVuService;
    private final CoQuanQuanLyService coQuanQuanLyService;

    public List<ChucVu2CoQuan2VaiTroDTO> findByCoQuanQuanLyId (long coQuanQuanLyId) {
        return chucVu2CoQuan2VaiTroMapper.toChucVu2CoQuan2VaiTroDTOList(chucVu2CoQuan2VaiTroRepository.findByCoQuanQuanLyId(coQuanQuanLyId));
    }

    public List<ChucVu2CoQuan2VaiTroDTO> findByCoQuanQuanLyIdAndSearchChucVu(long coQuanQuanLyId, String txtSearch) {
        List<Long> chucVuIds = chucVuService.getListChucVuIdsByTen(txtSearch);
        return chucVu2CoQuan2VaiTroMapper
                .toChucVu2CoQuan2VaiTroDTOList(chucVu2CoQuan2VaiTroRepository.findByCoQuanQuanLyIdAndChucVu_IdIn(coQuanQuanLyId, chucVuIds));
    }

    public void add(ChucVu2CoQuan2VaiTroDTO chucVu2CoQuan2VaiTroDTO) {

        ChucVu2CoQuan2VaiTro chucVu2CoQuan2VaiTro = chucVu2CoQuan2VaiTroMapper.chucVu2CoQuan2VaiTroDTOToChucVu2CoQuan2VaiTro(chucVu2CoQuan2VaiTroDTO);

        chucVu2CoQuan2VaiTroRepository.save(chucVu2CoQuan2VaiTro);
    }

    public void delete(long id) {

        chucVu2CoQuan2VaiTroRepository.deleteById(id);
    }

    public DataTablesOutput<ChucVu2CoQuan2VaiTro> findAll(DataTablesInput input) {

        return chucVu2CoQuan2VaiTroRepository.findAll(input);
    }

    public DataTablesOutput<ChucVu2CoQuan2VaiTro> getChucVu2CoQuan2VaiTros (long coQuanId, String search, int pageIndex, int size, int draw) {

        Pageable pageable = PageRequest.of(pageIndex, size);
        Page<BigInteger> chucVuByCoQuanIds = chucVu2CoQuan2VaiTroRepository.findChucVuByCoQuanId(coQuanId, search, pageable);

        List<ChucVu2CoQuan2VaiTro> chucVu2CoQuan2VaiTros = chucVu2CoQuan2VaiTroRepository.findByCoQuanQuanLyIdAndChucVu_IdIn(coQuanId,
                StringUtils.convertListBigIntToListLong(chucVuByCoQuanIds.getContent()));

        List<ChucVu2VaiTroDTO> chucVu2VaiTros = new ArrayList<>();

        if (chucVu2CoQuan2VaiTros.size() > 0) {

            Map<ChucVu, List<ChucVu2CoQuan2VaiTro>> map = chucVu2CoQuan2VaiTros.stream()
                    .collect(Collectors.groupingBy(ChucVu2CoQuan2VaiTro::getChucVu, Collectors.toList()));

            map.forEach((key, value) -> {
                ChucVu2VaiTroDTO chucVu2VaiTroDTO = new ChucVu2VaiTroDTO();
                chucVu2VaiTroDTO.setId(key.getId());
                chucVu2VaiTroDTO.setChucVu(key);
                chucVu2VaiTroDTO.setVaiTros(value.stream().map(
                        ChucVu2CoQuan2VaiTro -> ChucVu2CoQuan2VaiTro.getVaiTro()).collect(Collectors.toList()));

                chucVu2VaiTros.add(chucVu2VaiTroDTO);
            });
        }

        Page<ChucVu2VaiTroDTO> chucVu2VaiTroDTOPage = new PageImpl<>(chucVu2VaiTros,
                new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort()),
                chucVuByCoQuanIds.getTotalElements());

        return DatatableUtil.convertPageToDataTable (chucVu2VaiTroDTOPage, draw);
    }

    public List<ChucVu2CoQuan2VaiTro> findByCoQuanAndChucVu(long coQuanId, long chucVuId) {

        return chucVu2CoQuan2VaiTroRepository.findByCoQuanQuanLyIdAndChucVu_Id(coQuanId, chucVuId);
    }

    public List<Long> findChucVuByCoQuanId(long coQuanId) {

        Pageable pageable =  PageRequest.of(0, 100);

        return StringUtils.convertListBigIntToListLong(chucVu2CoQuan2VaiTroRepository.findChucVuByCoQuanId(coQuanId, "", pageable).getContent());
    }

    public List<ChucVuDTO> findChucVuByCoQuanQuanLyId(long coQuanId) {

        List<ChucVu> chucVus = new ArrayList<>();

        chucVus.addAll(chucVu2CoQuan2VaiTroRepository.findByCoQuanQuanLyId(coQuanId).stream().map(ChucVu2CoQuan2VaiTro::getChucVu).collect(Collectors.toList()));

        return chucVuMapper.chucVusToChucVuDTOs(chucVus);
    }

    public void deleteChucVu2VaiTroByCoQuan (long coQuanId, long chucVuId) {

        chucVu2CoQuan2VaiTroRepository.deleteByCoQuanQuanLyIdAndChucVu_Id(coQuanId, chucVuId);
    }

    public void createChucVu2VaiTroByCoQuan (long coQuanId, long chucVuId, List<VaiTro> vaiTros) {
        List<ChucVu2CoQuan2VaiTro> chucVu2CoQuan2VaiTros = new ArrayList<>();
        ChucVu chucVu = chucVuMapper.chucVuDTOToChucVu(chucVuService.getChucVu(chucVuId));

        if ((vaiTros != null && vaiTros.size() > 0)) {
            for (VaiTro vaiTro:vaiTros) {
                ChucVu2CoQuan2VaiTro chucVu2CoQuan2VaiTro = new ChucVu2CoQuan2VaiTro();
                chucVu2CoQuan2VaiTro.setChucVu(chucVu);
                chucVu2CoQuan2VaiTro.setCoQuanQuanLyId(coQuanId);
                chucVu2CoQuan2VaiTro.setVaiTro(vaiTro);

                chucVu2CoQuan2VaiTros.add(chucVu2CoQuan2VaiTro);
            }

            chucVu2CoQuan2VaiTroRepository.saveAll(chucVu2CoQuan2VaiTros);
        }
    }

    public List<Long> findRoleIdsByMaCoQuanAndMaChucVu (String maCoQuan, String maChucVu) {
        ChucVuDTO chucVuDTO = chucVuService.findFirstByMa(maChucVu);
        CoQuanQuanLyDTO coQuanQuanLyDTO  = coQuanQuanLyService.findFirstByMa(maCoQuan);
        List<Long> roleIds = new ArrayList<>();
        if (chucVuDTO != null && coQuanQuanLyDTO != null) {
            List<ChucVu2CoQuan2VaiTro> chucVu2CoQuan2VaiTros =
                    chucVu2CoQuan2VaiTroRepository.findByCoQuanQuanLyIdAndChucVu_Id(coQuanQuanLyDTO.getId(), chucVuDTO.getId());
            for (ChucVu2CoQuan2VaiTro chucVu2CoQuan2VaiTro : chucVu2CoQuan2VaiTros) {
                roleIds.addAll(chucVu2CoQuan2VaiTro.getVaiTro().getTaiNguyens().stream().map(TaiNguyen::getRoleId).collect(Collectors.toList()));
            }
        }

        return roleIds;
    }
}
