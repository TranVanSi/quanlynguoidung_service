package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.ChucVuRepository;
import sdt.osgi.springboot.dto.ChucVuDTO;
import sdt.osgi.springboot.mapper.ChucVuMapper;
import sdt.osgi.springboot.model.ChucVu;
import sdt.osgi.springboot.util.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ChucVuService {

    private final ChucVuRepository chucVuRepository;
    private final ChucVuMapper chucVuMapper;

    public Page<ChucVu> findAll(int page, int size){

        Pageable pageable = PageRequest.of(page, size);

        return chucVuRepository.findAll(pageable);
    }

    public DataTablesOutput<ChucVu> findAll(DataTablesInput input){

        return chucVuRepository.findAll(input);
    }

    public Page<ChucVu> findByKeyword(String keyword, int page, int size){

        Pageable pageable = PageRequest.of(page, size);

        return chucVuRepository.findByMaContainingOrTenContaining(keyword, pageable);
    }

    public List<ChucVu> findTop1000() {

        return chucVuRepository.findTop1000ByDaXoa(Constants.TAO_MOI);

    }

    public List<ChucVuDTO> findTop1000ByIdNotIn(List<Long> ids) {

        return chucVuMapper.chucVusToChucVuDTOs(chucVuRepository.findTop1000ByIdNotIn(ids));

    }

    public void add(ChucVuDTO chucVuDTO) {

        chucVuDTO.setDaXoa(Constants.TAO_MOI);
        ChucVu chucVu = chucVuMapper.chucVuDTOToChucVu(chucVuDTO);

        chucVuRepository.save(chucVu);
    }

    public void update(ChucVuDTO chucVuDTO) {
        ChucVu chucVuOrigin = chucVuRepository.findById(chucVuDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        ChucVu chucVu = ChucVu.builder()
                .id(chucVuDTO.getId())
                .ma(chucVuDTO.getMa() != null ? chucVuDTO.getMa() : chucVuOrigin.getMa())
                .ten(chucVuDTO.getTen() != null ? chucVuDTO.getTen() : chucVuOrigin.getTen())
                .daXoa(chucVuDTO.getDaXoa()).build();

        chucVuRepository.save(chucVu);
    }

    public ChucVuDTO getChucVu (long chucVuId) {

        Optional<ChucVu> chucVu = chucVuRepository.findById(chucVuId);

        return chucVuMapper.chucVuToChucVuDTO(chucVu.get());
    }

    public void delete(long chucVuId) {

        ChucVu chucVu = chucVuRepository.findById(chucVuId).get();
        chucVu.setDaXoa(Constants.DA_XOA);
        chucVuRepository.save(chucVu);
    }

    public ChucVuDTO findFirstByMa (String ma) {

        return chucVuMapper.chucVuToChucVuDTO(chucVuRepository.findFirstByMaAndDaXoa(ma, Constants.TAO_MOI));
    }

    public ChucVuDTO findFirstByTen (String ten) {

        return chucVuMapper.chucVuToChucVuDTO(chucVuRepository.findFirstByTen(ten));
    }

    public List<ChucVuDTO> getListChucVu() {

        return chucVuMapper.toChucVuDTOList(chucVuRepository.findByDaXoa(Constants.TAO_MOI));
    }

    public List<ChucVuDTO> getListChucVuBySearch(String txtSearch) {

        return chucVuMapper.toChucVuDTOList(chucVuRepository.findAllByTenContainingOrMaContainingAndDaXoa(txtSearch, txtSearch, Constants.TAO_MOI));
    }

    public void saveAllChucVu(List<ChucVuDTO> chucVuDTOS) {
        chucVuRepository.saveAll(chucVuMapper.toChucVuList(chucVuDTOS));
    }

    public List<Long> getListChucVuIdsByTen(String txtSearch){
        List<Long> chucVuIds = new ArrayList<>();
        List<ChucVu> chucVus = chucVuRepository.findByTenContaining(txtSearch);
        for(ChucVu  chucVu: chucVus){
            chucVuIds.add(chucVu.getId());
        }
        return  chucVuIds;
    }

}
