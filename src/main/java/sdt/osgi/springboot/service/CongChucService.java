package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.ChucVu2CoQuan2VaiTroRepository;
import sdt.osgi.springboot.dao.ChucVuRepository;
import sdt.osgi.springboot.dao.CongChucRepository;
import sdt.osgi.springboot.dto.CongChucDTO;
import sdt.osgi.springboot.dto.VaiTro2TaiNguyenDTO;
import sdt.osgi.springboot.mapper.*;
import sdt.osgi.springboot.model.ChucVu;
import sdt.osgi.springboot.model.ChucVu2CoQuan2VaiTro;
import sdt.osgi.springboot.model.CongChuc;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class CongChucService {

    private final CongChucRepository congChucRepository;
    private final CongChucMapper congChucMapper;
    private final TaiKhoanNguoiDungMapper taiKhoanNguoiDungMapper;
    private final ChucVuMapper chucVuMapper;
    private final ChucVuRepository chucVuRepository;
    private final CoQuanQuanLyMapper coQuanQuanLyMapper;
    private final ChucVu2CoQuan2VaiTroRepository chucVu2CoQuan2VaiTroRepository;
    private final VaiTroMapper vaiTroMapper;

    public List<CongChucDTO> getListCongChucByCoQuanQuanLyId(long coQuanQuanLyId) {
        List<CongChuc> congChucs;
        if (coQuanQuanLyId > 0) {
            congChucs = congChucRepository.findByCoQuanQuanLyId(coQuanQuanLyId);
        } else {
            congChucs =  congChucRepository.findByDaXoa(Constants.TAO_MOI);
        }
        
        List<CongChucDTO> congChucDTOS = congChucMapper.toListCongChucDTO(congChucs);
        return congChucDTOS;
    }

    public List<CongChucDTO> getListCongChuc() {

        return congChucMapper.toListCongChucDTO(congChucRepository.findByDaXoa(Constants.TAO_MOI));
    }

    public Page<CongChuc> findAll(int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        return congChucRepository.findAll(pageable);

    }

    public Page<CongChuc> findByKeyword(String keyword, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        return congChucRepository.findByMaContainingOrHoVaTenContainingOrEmailContaining(keyword, pageable);
    }

    public List<CongChuc> findTop1000() {

        return congChucRepository.findTop1000By();
    }

    public void add(CongChucDTO congChucDTO) {

        CongChuc congChuc = congChucMapper.congChucDTOToCongChuc(congChucDTO);
        ChucVu chucVuOrigin = chucVuRepository.findById(congChuc.getChucVu().getId()).get();
        congChuc.setChucVu(chucVuOrigin);
        congChucRepository.save(congChuc);
    }

    public void update(CongChucDTO congChucDTO) {

        CongChuc congChucOrigin = congChucRepository.findById(congChucDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        CongChuc congChuc = CongChuc.builder()
                .id(congChucDTO.getId())
                .email(congChucDTO.getEmail() != null ? congChucDTO.getEmail() : congChucOrigin.getEmail())
                .anh(congChucDTO.getAnh() != null ? congChucDTO.getAnh() : congChucOrigin.getAnh())
                .chucVu(congChucDTO.getChucVu() != null ? chucVuMapper.chucVuDTOToChucVu(congChucDTO.getChucVu()) : congChucOrigin.getChucVu())
                .coQuanQuanLy(congChucDTO.getCoQuanQuanLy() != null ? coQuanQuanLyMapper.toCoQuanQuanLy(congChucDTO.getCoQuanQuanLy()) : congChucOrigin.getCoQuanQuanLy())
                .dienThoaiDiDong(congChucDTO.getDienThoaiDiDong() != null ? congChucDTO.getDienThoaiDiDong() : congChucOrigin.getDienThoaiDiDong())
                .gioiTinhId(congChucDTO.getGioiTinhId())
                .hoVaTen(congChucDTO.getHoVaTen() != null ? congChucDTO.getHoVaTen() : congChucOrigin.getHoVaTen())
                .ma(congChucDTO.getMa() != null ? congChucDTO.getMa() : congChucOrigin.getMa())
                .soCMND(congChucDTO.getSoCMND() != null ? congChucDTO.getSoCMND() : congChucOrigin.getSoCMND())
                .ngayCapCMND(congChucDTO.getNgayCapCMND() != null ? congChucDTO.getNgayCapCMND() : congChucOrigin.getNgayCapCMND())
                .ngaySinh(congChucDTO.getNgaySinh() != null ? congChucDTO.getNgaySinh() : congChucOrigin.getNgaySinh())
                .noiCapId(congChucDTO.getNoiCapId())
                .taiKhoanNguoiDung(taiKhoanNguoiDungMapper.taiKhoanNguoiDungDTOToTaiKhoanNguoiDung(congChucDTO.getTaiKhoanNguoiDung()))
                .daXoa(Constants.TAO_MOI)
                .build();

        congChuc.setNgayTao(congChucOrigin.getNgayTao());
        congChucRepository.save(congChuc);
    }

    public CongChucDTO getCongChuc(long congChucId) {
        Optional<CongChuc> congChuc = congChucRepository.findById(congChucId);

        return congChucMapper.congChucToCongChucDTO(congChuc.get());
    }

    public void delete(long congChucId) {
        CongChuc congChuc = congChucRepository.findById(congChucId).get();
        congChuc.setDaXoa(Constants.DA_XOA);
        congChucRepository.save(congChuc);
    }

    public CongChucDTO findFirstByMa(String ma) {

        return congChucMapper.congChucToCongChucDTO(congChucRepository.findFirstByMaAndDaXoa(ma, Constants.TAO_MOI));
    }

    public CongChucDTO findByTaiKhoanNguoiDungId(long taiKhoanNguoiDungId) {

        return congChucMapper.congChucToCongChucDTO(congChucRepository.findFirstByTaiKhoanNguoiDungIdAndDaXoa(taiKhoanNguoiDungId, Constants.TAO_MOI));
    }

    public Long findByEmailNguoiDung(String email) {
        CongChuc congChuc = congChucRepository.findByTaiKhoanNguoiDung_Email(email);
        return congChuc != null ? congChuc.getCoQuanQuanLy().getId() : Long.valueOf(0);
    }

    public DataTablesOutput<CongChuc> findAll(DataTablesInput input) {

        return congChucRepository.findAll(input);
    }

    public Page<CongChuc> findToPaging(long coQuanQuanLyId, String name, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (coQuanQuanLyId > 0) {
            if (name != null && name.trim().length() > 0) {
                return congChucRepository.findByCoQuanQuanLy_IdAndMaContainingAndDaXoaOrCoQuanQuanLy_IdAndHoVaTenContainingAndDaXoaOrCoQuanQuanLy_IdAndEmailContainingAndDaXoa
                        (coQuanQuanLyId, name, Constants.TAO_MOI, coQuanQuanLyId, name, Constants.TAO_MOI, coQuanQuanLyId, name, Constants.TAO_MOI, pageable);
            } else {
                return congChucRepository.findByCoQuanQuanLy_IdAndDaXoa(coQuanQuanLyId, Constants.TAO_MOI, pageable);
            }
        } else {
            if (name != null && name.trim().length() > 0) {
                return congChucRepository.findByMaContainingAndDaXoaOrHoVaTenContainingAndDaXoaOrEmailContainingAndDaXoa(name, Constants.TAO_MOI, name, Constants.TAO_MOI, name, Constants.TAO_MOI, pageable);
            } else {
                return congChucRepository.findByDaXoa(Constants.TAO_MOI, pageable);
            }
        }
    }

    public List<CongChucDTO> getCongChucByCoQuanAndTaiNguyenId(long coQuanQuanLyId, long taiNguyenId) {
        return congChucMapper.toListCongChucDTO(congChucRepository.findAllByCoQuanAndTaiNguyen(coQuanQuanLyId, taiNguyenId));
    }

    public List<VaiTro2TaiNguyenDTO> getPermission(long congChucId) {
        Optional<CongChuc> congChuc = congChucRepository.findById(congChucId);
        List<ChucVu2CoQuan2VaiTro> chucVu2CoQuan2VaiTros =
                chucVu2CoQuan2VaiTroRepository.findByCoQuanQuanLyIdAndChucVu_Id(congChuc.get().getCoQuanQuanLy().getId(), congChuc.get().getChucVu().getId());
        List<VaiTro2TaiNguyenDTO> vaiTroDTOS = new ArrayList<>();

        for (ChucVu2CoQuan2VaiTro chucVu2CoQuan2VaiTro : chucVu2CoQuan2VaiTros) {
            vaiTroDTOS.add(vaiTroMapper.toVaiTro2TaiNguyen(chucVu2CoQuan2VaiTro.getVaiTro()));
        }

        return vaiTroDTOS;
    }

    public List<CongChucDTO> getListCongChucByChucVuId(long chucVuId) {
        List<CongChuc> congChucs = congChucRepository.findByChucVu_IdAndDaXoa(chucVuId, Constants.TAO_MOI);

        return congChucMapper.toListCongChucDTO(congChucs);
    }

    public List<CongChucDTO> getListCongChucByCQQLIds(String coQuanQuanLyIds) {
        List<Long> listCoQuan = StringUtils.convertStringToLongs(coQuanQuanLyIds);
        List<CongChuc> congChucs = congChucRepository.findByCoQuanQuanLyIdIn(listCoQuan);
        return congChucMapper.toListCongChucDTO(congChucs);
    }

    public List<CongChucDTO> findAllByIdIn(String IdStr) {
        List<Long> ids = Stream.of(IdStr.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());
        return congChucMapper.toListCongChucDTO(congChucRepository.findAllByIdIn(ids));
    }
}
