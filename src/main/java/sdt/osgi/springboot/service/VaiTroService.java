package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.VaiTroRepository;
import sdt.osgi.springboot.dto.VaiTroDTO;
import sdt.osgi.springboot.mapper.VaiTroMapper;
import sdt.osgi.springboot.model.VaiTro;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class VaiTroService {

    private final VaiTroRepository vaiTroRepository;
    private final VaiTroMapper vaiTroMapper;

    public List<VaiTroDTO> getListVaiTro() {
        return vaiTroMapper.toVaiTroDTOList(vaiTroRepository.findAll());
    }

    public List<VaiTroDTO> getListVaiTroBySearch(String keyword) {
        return vaiTroMapper.toVaiTroDTOList(vaiTroRepository.findAllByTenContainingOrMaContaining(keyword, keyword));
    }

    public Page<VaiTroDTO> findAll(int page, int size){

        Pageable pageable = PageRequest.of(page, size);

        Page<VaiTro> vaiTros = vaiTroRepository.findAllByOrderByNgaySuaDesc(pageable);

        List<VaiTroDTO> vaiTroDTOS = vaiTroMapper.vaiTrosToVaiTroDTOs(vaiTros.getContent());

        Page<VaiTroDTO> vaiTroDTOPage = new PageImpl<>(vaiTroDTOS,
                new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort()),
                vaiTros.getTotalElements());

        return vaiTroDTOPage;

    }

    public Page<VaiTroDTO> findByKeyword(String keyword, int page, int size){

        Pageable pageable = PageRequest.of(page, size);

        Page<VaiTro> vaiTros = vaiTroRepository.findByMaContainingOrTenContaining(keyword, pageable);
        List<VaiTroDTO> vaiTroDTOS = vaiTroMapper.vaiTrosToVaiTroDTOs(vaiTros.getContent());

        Page<VaiTroDTO> vaiTroDTOPage = new PageImpl<>(vaiTroDTOS,
                new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort()),
                vaiTros.getTotalElements());

        return vaiTroDTOPage;

    }

    public List<VaiTro> findTop1000(){

        return vaiTroRepository.findTop1000By();
    }

    public void add(VaiTroDTO vaiTroDTO) {

        VaiTro vaiTro = vaiTroMapper.vaiTroDTOToVaiTro(vaiTroDTO);

        vaiTroRepository.save(vaiTro);

    }

    public void update(VaiTroDTO vaiTroDTO) {
        VaiTro vaiTroOrigin = vaiTroRepository.findById(vaiTroDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay vai tro!!!"));

        VaiTro vaiTro = VaiTro.builder()
                .id(vaiTroDTO.getId())
                .ma(vaiTroDTO.getMa() != null ? vaiTroDTO.getMa() : vaiTroOrigin.getMa())
                .moTa(vaiTroDTO.getMoTa() != null ? vaiTroDTO.getMoTa() : "")
                .ten(vaiTroDTO.getTen() != null ? vaiTroDTO.getTen() : vaiTroOrigin.getTen())
                .taiNguyens(vaiTroDTO.getTaiNguyens() != null ? vaiTroDTO.getTaiNguyens() : vaiTroOrigin.getTaiNguyens())
                .trangThai(vaiTroDTO.getTrangThai() != null ? vaiTroDTO.getTrangThai() : vaiTroOrigin.getTrangThai()).build();

        vaiTroRepository.save(vaiTro);
    }

    public VaiTroDTO getVaiTro (long vaiTroId) {

        Optional<VaiTro> vaiTro = vaiTroRepository.findById(vaiTroId);

        return vaiTroMapper.vaiTroToVaiTroDTO(vaiTro.get());
    }

    public void delete(long vaiTroId) {

        vaiTroRepository.deleteById(vaiTroId);
    }

    public VaiTroDTO findFirstByMa (String ma) {

        return vaiTroMapper.vaiTroToVaiTroDTO(vaiTroRepository.findFirstByMa(ma));
    }

    public VaiTroDTO findFirstByTen (String ten) {

        return vaiTroMapper.vaiTroToVaiTroDTO(vaiTroRepository.findFirstByTen(ten));
    }

    public List<VaiTroDTO> findByVaiTroIsNotIn(List<Long> ids) {

        return vaiTroMapper.vaiTrosToVaiTroDTOs(vaiTroRepository.findByIdNotIn(ids)) ;

    }

    public List<VaiTroDTO> findByVaiTroIn(List<Long> ids) {

        return vaiTroMapper.vaiTrosToVaiTroDTOs(vaiTroRepository.findByIdIn(ids)) ;

    }

    public List<VaiTroDTO> findByVaiTroIsNotInAndTrangThai(List<Long> ids, Integer trangThai) {

        return vaiTroMapper.vaiTrosToVaiTroDTOs(vaiTroRepository.findByIdNotInAndTrangThai(ids, trangThai)) ;

    }

    public List<VaiTro> findByTrangThai(Integer trangThai) {

        return vaiTroRepository.findByTrangThai(trangThai) ;

    }
}
