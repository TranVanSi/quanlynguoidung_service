package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.DonViHanhChinhRepository;
import sdt.osgi.springboot.dto.DonViHanhChinhDTO;
import sdt.osgi.springboot.mapper.DonViHanhChinhMapper;
import sdt.osgi.springboot.util.Constants;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DonViHanhChinhService {
    private final DonViHanhChinhMapper donViHanhChinhMapper;
    private final DonViHanhChinhRepository donViHanhChinhRepository;

    public List<DonViHanhChinhDTO> findByCapCoQuanId(long capId) {
        return donViHanhChinhMapper.toDonViHanhChinhDTOList(donViHanhChinhRepository.findByCapDonViHanhChinh_IdAndDaXoa(capId, Constants.TAO_MOI));
    }

}
