package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.ChucVu2CoQuan2VaiTroRepository;
import sdt.osgi.springboot.dao.TaiKhoanNguoiDungRepository;
import sdt.osgi.springboot.dto.CongChucDTO;
import sdt.osgi.springboot.dto.NguoiDung2VaiTroDTO;
import sdt.osgi.springboot.dto.TaiKhoanNguoiDungDTO;
import sdt.osgi.springboot.dto.TaiNguyenDTO;
import sdt.osgi.springboot.mapper.LoaiDoiTuongMapper;
import sdt.osgi.springboot.mapper.TaiKhoanNguoiDungMapper;
import sdt.osgi.springboot.mapper.TaiNguyenMapper;
import sdt.osgi.springboot.model.ChucVu2CoQuan2VaiTro;
import sdt.osgi.springboot.model.NguoiDung2VaiTro;
import sdt.osgi.springboot.model.TaiKhoanNguoiDung;
import sdt.osgi.springboot.model.VaiTro;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class TaiKhoanNguoiDungService {

    private final TaiKhoanNguoiDungRepository taiKhoanNguoiDungRepository;
    private final TaiKhoanNguoiDungMapper taiKhoanNguoiDungMapper;
    private final NguoiDung2VaiTroService nguoiDung2VaiTroService;
    private final ChucVu2CoQuan2VaiTroRepository chucVu2CoQuan2VaiTroRepository;
    private final LoaiDoiTuongMapper loaiDoiTuongMapper;

    public Page<TaiKhoanNguoiDung> findAll(int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        return taiKhoanNguoiDungRepository.findAll(pageable);

    }

    public List<TaiKhoanNguoiDung> findTop1000() {

        return taiKhoanNguoiDungRepository.findTop1000By();
    }

    public void add(TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO) {

        TaiKhoanNguoiDung taiKhoanNguoiDung = taiKhoanNguoiDungMapper.taiKhoanNguoiDungDTOToTaiKhoanNguoiDung(taiKhoanNguoiDungDTO);

        taiKhoanNguoiDungRepository.save(taiKhoanNguoiDung);
    }

    public TaiKhoanNguoiDung update(TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO) {
        TaiKhoanNguoiDung taiKhoanNguoiDungOrigin = taiKhoanNguoiDungRepository.findById(taiKhoanNguoiDungDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        TaiKhoanNguoiDung taiKhoanNguoiDung = TaiKhoanNguoiDung.builder()
                .id(taiKhoanNguoiDungDTO.getId())
                .email(taiKhoanNguoiDungDTO.getEmail() != null ? taiKhoanNguoiDungDTO.getEmail() : taiKhoanNguoiDungOrigin.getEmail())
                .matKhau(taiKhoanNguoiDungDTO.getMatKhau() != null ? taiKhoanNguoiDungDTO.getMatKhau() : taiKhoanNguoiDungOrigin.getMatKhau())
                .tenDangNhap(taiKhoanNguoiDungDTO.getTenDangNhap() != null ? taiKhoanNguoiDungDTO.getTenDangNhap() : taiKhoanNguoiDungOrigin.getTenDangNhap())
                .tenNguoiDung(taiKhoanNguoiDungDTO.getTenNguoiDung() != null ? taiKhoanNguoiDungDTO.getTenNguoiDung() : taiKhoanNguoiDungOrigin.getTenNguoiDung())
                .userId(taiKhoanNguoiDungDTO.getUserId() != null ? taiKhoanNguoiDungDTO.getUserId() : taiKhoanNguoiDungOrigin.getUserId())
                .loaiDoiTuong(taiKhoanNguoiDungDTO.getLoaiDoiTuong() != null ? loaiDoiTuongMapper.loaiDoiTuongDTOToLoaiDoiTuong(taiKhoanNguoiDungDTO.getLoaiDoiTuong()) : taiKhoanNguoiDungOrigin.getLoaiDoiTuong())
                .trangThai(taiKhoanNguoiDungDTO.getTrangThai() != null ? taiKhoanNguoiDungDTO.getTrangThai() : taiKhoanNguoiDungOrigin.getTrangThai()).build();

        return taiKhoanNguoiDungRepository.save(taiKhoanNguoiDung);
    }

    public TaiKhoanNguoiDungDTO getTaiKhoanNguoiDung(long taiKhoanNguoiDungId) {

        Optional<TaiKhoanNguoiDung> taiKhoanNguoiDung = taiKhoanNguoiDungRepository.findById(taiKhoanNguoiDungId);

        return taiKhoanNguoiDungMapper.taiKhoanNguoiDungToTaiKhoanNguoiDungDTO(taiKhoanNguoiDung.get());
    }

    public void delete(long taiKhoanNguoiDungId) {

        taiKhoanNguoiDungRepository.deleteById(taiKhoanNguoiDungId);
    }

    public TaiKhoanNguoiDungDTO findFirstByEmail(String email) {

        return taiKhoanNguoiDungMapper.taiKhoanNguoiDungToTaiKhoanNguoiDungDTO(taiKhoanNguoiDungRepository.findFirstByEmail(email));
    }

    public TaiKhoanNguoiDungDTO findByUserId(Long userId) {

        return taiKhoanNguoiDungMapper.taiKhoanNguoiDungToTaiKhoanNguoiDungDTO(taiKhoanNguoiDungRepository.findByUserId(userId));
    }


    public List<Long> getListRoleIds(String email) {

        List<Long> listRoleIds = new ArrayList<>();

        TaiKhoanNguoiDung taiKhoanNguoiDung = taiKhoanNguoiDungRepository.findFirstByEmail(email);

        if (taiKhoanNguoiDung != null && taiKhoanNguoiDung.getCongChuc() != null) {
            List<ChucVu2CoQuan2VaiTro> chucVu2CoQuan2VaiTros = chucVu2CoQuan2VaiTroRepository.findByCoQuanQuanLyIdAndChucVu_Id(taiKhoanNguoiDung.getCongChuc().getCoQuanQuanLy().getId(), taiKhoanNguoiDung.getCongChuc().getChucVu().getId());
            List<NguoiDung2VaiTroDTO> nguoiDung2VaiTroList = nguoiDung2VaiTroService.findAllByCongChucId(taiKhoanNguoiDung.getCongChuc().getId());
            if (chucVu2CoQuan2VaiTros != null) {
                chucVu2CoQuan2VaiTros.forEach(item ->
                        item.getVaiTro().getTaiNguyens().forEach(taiNguyen -> listRoleIds.add(taiNguyen.getRoleId()))
                );
            }
            if (nguoiDung2VaiTroList != null) {
                nguoiDung2VaiTroList.forEach(item ->
                        item.getVaiTro().getTaiNguyens().forEach(taiNguyen -> listRoleIds.add(taiNguyen.getRoleId()))
                );
            }
        }

        if (listRoleIds.isEmpty()) {
            listRoleIds.add(1l);
            listRoleIds.add(2l);
        }
        return listRoleIds;
    }
}
