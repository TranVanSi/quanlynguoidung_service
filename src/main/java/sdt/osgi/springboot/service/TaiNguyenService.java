package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.TaiNguyenRepository;
import sdt.osgi.springboot.dto.TaiNguyenDTO;
import sdt.osgi.springboot.mapper.TaiNguyenMapper;
import sdt.osgi.springboot.model.TaiNguyen;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TaiNguyenService {

    private final TaiNguyenRepository taiNguyenRepository;
    private final TaiNguyenMapper taiNguyenMapper;

    public List<TaiNguyenDTO> getListTaiNguyen(){

        return taiNguyenMapper.toTaiNguyenDTOList(taiNguyenRepository.findAll());

    }

    public List<TaiNguyenDTO> getListTaiNguyenBySearch(String keyword){

        return taiNguyenMapper.toTaiNguyenDTOList(taiNguyenRepository.findAllByTenContainingOrMaContaining(keyword, keyword));

    }

    public Page<TaiNguyen> findAll(int page, int size){

        Pageable pageable = PageRequest.of(page, size);

        return taiNguyenRepository.findAllByOrderByNgaySuaDesc(pageable);

    }

    public Page<TaiNguyen> findByKeyword(String keyword, int page, int size){

        Pageable pageable = PageRequest.of(page, size);

        return taiNguyenRepository.findByMaContainingOrTenContaining(keyword, pageable);

    }

    public List<TaiNguyen> findTop1000(){

        return taiNguyenRepository.findTop1000By();
    }
    public List<TaiNguyen> findByTrangThai(Integer trangThai){

        return taiNguyenRepository.findByTrangThai(trangThai);
    }

    public void add(TaiNguyenDTO taiNguyenDTO) {

        TaiNguyen taiNguyen = taiNguyenMapper.taiNguyenDTOToTaiNguyen(taiNguyenDTO);

        taiNguyenRepository.save(taiNguyen);
    }

    public void update(TaiNguyenDTO taiNguyenDTO) {
        TaiNguyen taiNguyenOrigin = taiNguyenRepository.findById(taiNguyenDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        TaiNguyen taiNguyen = TaiNguyen.builder()
                .id(taiNguyenDTO.getId())
                .loai(taiNguyenDTO.getLoai() != null ? taiNguyenDTO.getLoai() : taiNguyenOrigin.getLoai())
                .ma(taiNguyenDTO.getMa() != null ? taiNguyenDTO.getMa() : taiNguyenOrigin.getMa())
                .moTa(taiNguyenDTO.getMoTa() != null ? taiNguyenDTO.getMoTa() : "")
                .roleId(taiNguyenDTO.getRoleId() != null ? taiNguyenDTO.getRoleId() : taiNguyenOrigin.getRoleId())
                .ten(taiNguyenDTO.getTen() != null ? taiNguyenDTO.getTen() : taiNguyenOrigin.getTen())
                .trangThai(taiNguyenDTO.getTrangThai() != null ? taiNguyenDTO.getTrangThai() : taiNguyenOrigin.getTrangThai()).build();

        taiNguyenRepository.save(taiNguyen);
    }

    public TaiNguyenDTO getTaiNguyen (long taiNguyenId) {

        Optional<TaiNguyen> taiNguyen = taiNguyenRepository.findById(taiNguyenId);

        return taiNguyenMapper.taiNguyenToTaiNguyenDTO(taiNguyen.get());
    }

    public void delete(long taiNguyenId) {

        taiNguyenRepository.deleteById(taiNguyenId);
    }

    public TaiNguyenDTO findFirstByMa (String ma) {

        return taiNguyenMapper.taiNguyenToTaiNguyenDTO(taiNguyenRepository.findFirstByMa(ma));
    }

    public TaiNguyenDTO findFirstByTen (String ten) {

        return taiNguyenMapper.taiNguyenToTaiNguyenDTO(taiNguyenRepository.findFirstByTen(ten));
    }

    public List<TaiNguyenDTO> findByTaiNguyenIsNotIn(List<Long> ids) {

        return taiNguyenMapper.taiNguyensTotaiNguyenDTOs(taiNguyenRepository.findByIdNotIn(ids)) ;
    }

    public List<TaiNguyenDTO> findByTaiNguyenIsNotInAndTrangThai(List<Long> ids, Integer trangThai) {

        return taiNguyenMapper.taiNguyensTotaiNguyenDTOs(taiNguyenRepository.findByIdNotInAndTrangThai(ids, trangThai)) ;
    }

    public List<TaiNguyenDTO> findByTaiNguyenIn(List<Long> ids) {

        return taiNguyenMapper.taiNguyensTotaiNguyenDTOs(taiNguyenRepository.findByIdIn(ids)) ;

    }

    public String findAllTaiNguyenByCoQuanIdAndChucVuId(long coQuanId, long chucVuId) {
        List<Long> taiNguyenIds = taiNguyenRepository.findAllTaiNguyenIdByCoQuanIdAndChucVuId(coQuanId, chucVuId);
        return taiNguyenIds.stream().map(String::valueOf).collect(Collectors.joining(","));
    }
}
