package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import sdt.osgi.springboot.controller.UploadValidate;
import sdt.osgi.springboot.dao.ChucVuRepository;
import sdt.osgi.springboot.dto.ChucVuDTO;
import sdt.osgi.springboot.mapper.ChucVuMapper;
import sdt.osgi.springboot.util.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
@RequiredArgsConstructor
public class UploadFileService {
    private final ChucVuService chucVuService;
    private final ChucVuMapper chucVuMapper;
    private final ChucVuRepository chucVuRepository;
    private final UploadValidate uploadValidate;

    public void saveAllChucVu (MultipartFile file, String nguoiTao) throws Exception {

        List<ChucVuDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        int i = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            ChucVuDTO chucVuDTO = new ChucVuDTO();
            if (i != 0) {
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    if (cell.getColumnIndex() == 0) {
                        chucVuDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                        continue;
                    }

                    if (cell.getColumnIndex() == 1) {
                        chucVuDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                        break;
                    }
                }

                ChucVuDTO itemByMa = chucVuMapper.chucVuToChucVuDTO(chucVuRepository.findFirstByMaAndDaXoa(chucVuDTO.getMa(), Constants.TAO_MOI) );
                if (uploadValidate.validateChucVu(chucVuDTO, itemByMa)) {
                    chucVuDTO.setNguoiTao(nguoiTao);
                    chucVuDTO.setNguoiSua(nguoiTao);
                    chucVuDTO.setDaXoa(Constants.TAO_MOI);
                    chucVuDTO.setNgayTao(new Date());
                    dtoList.add(chucVuDTO);
                }
            }
            i++;
        }

        chucVuService.saveAllChucVu(dtoList);
    }
}
