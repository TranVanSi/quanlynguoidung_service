package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class CoQuanQuanLy extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String ma;
    private String ten;
    private String diaChi;
    @ManyToOne
    @JoinColumn(name = "capCoQuanQuanLyId")
    private CapCoQuanQuanLy capCoQuanQuanLy;

    private Long chaId;
    private Integer daKhoa;
    private int trangThai;

    public CoQuanQuanLy() {

    }
}
