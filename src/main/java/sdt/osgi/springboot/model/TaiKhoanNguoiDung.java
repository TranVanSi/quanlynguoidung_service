package sdt.osgi.springboot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class TaiKhoanNguoiDung extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String tenDangNhap;

    private String matKhau;

    private String tenNguoiDung;

    private String email;

    private Integer trangThai;

    private Long userId;

    @ManyToOne
    @JoinColumn(name = "loaiDoiTuongId")
    private LoaiDoiTuong loaiDoiTuong;

    @OneToOne(mappedBy = "taiKhoanNguoiDung", cascade = CascadeType.ALL)
    @JsonIgnore
    private CongChuc congChuc;

    public TaiKhoanNguoiDung() {
    }
}
