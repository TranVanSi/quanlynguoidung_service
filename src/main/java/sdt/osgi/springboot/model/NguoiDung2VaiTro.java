package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NguoiDung2VaiTro extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "congChucId")
    private CongChuc congChuc;

    @ManyToOne
    @JoinColumn(name = "vaiTroId")
    private VaiTro vaiTro;
}
