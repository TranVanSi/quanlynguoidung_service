package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@Builder
public class CapDonViHanhChinh extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private String moTa;
    private int cap;
    @OneToMany(mappedBy = "capDonViHanhChinh", cascade= CascadeType.ALL)
    private List<DonViHanhChinh> donViHanhChinhs;
    private int daXoa;

    public CapDonViHanhChinh() {

    }

}
