package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@Builder
public class CongChuc extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String ma;

    private String hoVaTen;

    private Date ngaySinh;

    private String soCMND;

    private Date ngayCapCMND;

    private Long noiCapId;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "taiKhoanNguoiDungId")
    private TaiKhoanNguoiDung taiKhoanNguoiDung;

    @ManyToOne
    @JoinColumn(name = "chucVuId")
    private ChucVu chucVu;

    private Long gioiTinhId;

    @ManyToOne
    @JoinColumn(name = "coQuanQuanLyId")
    private CoQuanQuanLy coQuanQuanLy;

    private String anh;

    private String dienThoaiDiDong;

    private String email;

    private Integer daXoa;

    public CongChuc() {
    }
}
