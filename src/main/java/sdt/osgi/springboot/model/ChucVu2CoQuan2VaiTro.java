package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@Builder
public class ChucVu2CoQuan2VaiTro {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long chucVu2CoQuan2VaiTroId;

    private Long coQuanQuanLyId;

    @ManyToOne
    @JoinColumn(name = "vaiTroId")
    private VaiTro vaiTro;

    @ManyToOne
    @JoinColumn(name = "chucVuId")
    private ChucVu chucVu;

    public ChucVu2CoQuan2VaiTro() {
    }
}
