package sdt.osgi.springboot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class DonViHanhChinh extends Auditable<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private Long chaId;
    private String chaMa;
    @ManyToOne(cascade = {
            CascadeType.MERGE,
            CascadeType.REFRESH
    })
    @JoinColumn(name = "capDonViHanhChinhId")
    @JsonIgnore
    private CapDonViHanhChinh capDonViHanhChinh;
    private String maBuuCuc;
    private int daXoa;

    public DonViHanhChinh() {

    }
}
