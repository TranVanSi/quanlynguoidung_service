package sdt.osgi.springboot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@Builder
public class TaiNguyen extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String ma;

    private String ten;

    private Integer loai;

    private Long roleId;

    private Integer trangThai;

    private String moTa;

    @ManyToMany
    @JoinTable(name = "TaiNguyen2VaiTro", joinColumns = @JoinColumn(name = "taiNguyenId"),
            inverseJoinColumns = @JoinColumn(name = "vaiTroId"))
    @JsonIgnore
    private List<VaiTro> vaiTros;

    private Long ungDungTichHopId;

    public TaiNguyen() {
    }
}
