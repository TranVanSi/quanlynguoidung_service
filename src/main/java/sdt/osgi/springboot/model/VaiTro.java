package sdt.osgi.springboot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@Builder
public class VaiTro extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String ma;

    private String ten;

    private Integer trangThai;

    private String moTa;

    @ManyToMany
    @JoinTable(name = "TaiNguyen2VaiTro", joinColumns = @JoinColumn(name = "vaiTroId"),
            inverseJoinColumns = @JoinColumn(name = "taiNguyenId"))
    @JsonIgnore
    private List<TaiNguyen> taiNguyens;

    public VaiTro() {
    }
}
