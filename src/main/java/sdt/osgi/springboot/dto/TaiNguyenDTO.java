package sdt.osgi.springboot.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.Auditable;
import sdt.osgi.springboot.model.VaiTro;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Builder
@Accessors(chain = true)
public class TaiNguyenDTO extends Auditable<String> implements Serializable {
    private Long id;
    private String ma;
    private String ten;
    private Integer loai;
    private Long roleId;
    private Integer trangThai;
    private String moTa;
    private Long ungDungTichHopId;
}
