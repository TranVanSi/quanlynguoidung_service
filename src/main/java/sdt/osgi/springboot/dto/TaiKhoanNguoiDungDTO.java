package sdt.osgi.springboot.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.Auditable;

import java.io.Serializable;

@Data
@Builder
@Accessors(chain = true)
public class TaiKhoanNguoiDungDTO {
    private Long id;
    private String tenDangNhap;
    private String matKhau;
    private String tenNguoiDung;
    private String cauHoiMatKhau;
    private String cauTraLoiMatKhau;
    private String email;
    private Integer trangThai;
    private Long userId;
    private LoaiDoiTuongDTO loaiDoiTuong;
}
