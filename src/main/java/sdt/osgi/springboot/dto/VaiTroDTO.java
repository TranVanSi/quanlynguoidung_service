package sdt.osgi.springboot.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.Auditable;
import sdt.osgi.springboot.model.ChucVu2CoQuan2VaiTro;
import sdt.osgi.springboot.model.TaiNguyen;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@Accessors(chain = true)
public class VaiTroDTO extends Auditable<String> implements Serializable {
    private Long id;
    private String ma;
    private String ten;
    private Integer trangThai;
    private String moTa;
    private List<TaiNguyen> taiNguyens;
    private List<ChucVu2CoQuan2VaiTro> chucVu2CoQuan2VaiTros;
}
