package sdt.osgi.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.TaiNguyen;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class VaiTro2TaiNguyenDTO {
    private Long id;
    private String ma;
    private String ten;
    private Integer trangThai;
    private String moTa;
    private List<TaiNguyen> taiNguyens;

    public VaiTro2TaiNguyenDTO() {
    }
}
