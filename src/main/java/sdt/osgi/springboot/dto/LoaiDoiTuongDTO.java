package sdt.osgi.springboot.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.Auditable;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@Accessors(chain = true)
public class LoaiDoiTuongDTO {
    private Long id;
    private String ma;
    private String ten;
    private String moTa;
    private Integer trangThai;
    private List<TaiKhoanNguoiDungDTO> taiKhoanNguoiDungDTOList;
}
