package sdt.osgi.springboot.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.ChucVu;
import sdt.osgi.springboot.model.VaiTro;

@Data
@Builder
@Accessors(chain = true)
public class ChucVu2CoQuan2VaiTroDTO {
    private Long id;
    private Long coQuanQuanLyId;
    private VaiTro vaiTro;
    private ChucVu chucVu;
}
