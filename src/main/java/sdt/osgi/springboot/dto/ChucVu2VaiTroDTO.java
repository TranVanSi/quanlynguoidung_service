package sdt.osgi.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.ChucVu;
import sdt.osgi.springboot.model.VaiTro;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class ChucVu2VaiTroDTO {
    private long id;
    private ChucVu chucVu;
    private List<VaiTro> vaiTros;

    public ChucVu2VaiTroDTO() {
    }
}
