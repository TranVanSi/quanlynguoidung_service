package sdt.osgi.springboot.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.Auditable;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@Accessors(chain = true)
public class CoQuanQuanLyDTO extends Auditable<String> implements Serializable {
    private Long id;
    private String ma;
    private String ten;
    private String diaChi;
    private CapCoQuanQuanLyDTO capCoQuanQuanLy;
    private List<CongChucDTO> congChucs;
    private long chaId;
    private int trangThai;

    private int daKhoa;
}
