package sdt.osgi.springboot.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@Accessors(chain = true)
public class CongChucDTO {
    private long id;
    private String ma;
    private String hoVaTen;
    private Date ngaySinh;
    private String soCMND;
    private Date ngayCapCMND;
    private long noiCapId;
    private TaiKhoanNguoiDungDTO taiKhoanNguoiDung;
    private ChucVuDTO chucVu;
    private long gioiTinhId;
    private CoQuanQuanLyDTO coQuanQuanLy;
    private String anh;
    private String dienThoaiDiDong;
    private String email;

    private int daXoa;
}
