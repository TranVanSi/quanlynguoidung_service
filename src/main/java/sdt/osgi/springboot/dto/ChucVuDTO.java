package sdt.osgi.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class ChucVuDTO {
    private long id;
    private String ma;
    private String ten;
    private List<CongChucDTO> congChucs;
    private int daXoa;

    private String nguoiTao;
    private String nguoiSua;

    private Date ngayTao;
    public ChucVuDTO() {
    }
}
