package sdt.osgi.springboot.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.Auditable;
import sdt.osgi.springboot.model.CongChuc;
import sdt.osgi.springboot.model.VaiTro;

import java.io.Serializable;

@Data
@Builder
@Accessors(chain = true)
public class NguoiDung2VaiTroDTO extends Auditable<String> implements Serializable {
    private Long id;
    private CongChuc congChuc;
    private VaiTro vaiTro;
}