package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.TaiKhoanNguoiDungDTO;
import sdt.osgi.springboot.model.TaiKhoanNguoiDung;

@Mapper(componentModel = "spring")
public interface TaiKhoanNguoiDungMapper {
    TaiKhoanNguoiDung taiKhoanNguoiDungDTOToTaiKhoanNguoiDung(TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO);
    TaiKhoanNguoiDungDTO taiKhoanNguoiDungToTaiKhoanNguoiDungDTO(TaiKhoanNguoiDung taiKhoanNguoiDung);
}
