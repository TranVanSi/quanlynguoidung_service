package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.LoaiDoiTuongDTO;
import sdt.osgi.springboot.model.LoaiDoiTuong;

@Mapper(componentModel = "spring")
public interface LoaiDoiTuongMapper {
    LoaiDoiTuong loaiDoiTuongDTOToLoaiDoiTuong(LoaiDoiTuongDTO loaiDoiTuongDTO);
    LoaiDoiTuongDTO loaiDoiTuongToLoaiDoiTuongDTO(LoaiDoiTuong loaiDoiTuong);
}
