package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.CongChucDTO;
import sdt.osgi.springboot.model.CongChuc;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CongChucMapper {
    CongChuc congChucDTOToCongChuc(CongChucDTO congChucDTO);
    CongChucDTO congChucToCongChucDTO(CongChuc congChuc);
    List<CongChucDTO> toListCongChucDTO(Iterable<CongChuc> congChucs);
}
