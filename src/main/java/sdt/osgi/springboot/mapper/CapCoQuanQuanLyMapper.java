package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.CapCoQuanQuanLyDTO;
import sdt.osgi.springboot.model.CapCoQuanQuanLy;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CapCoQuanQuanLyMapper {
    CapCoQuanQuanLyDTO toCapCoQuanQuanLyDTO(CapCoQuanQuanLy CapCoQuanQuanLy);

    List<CapCoQuanQuanLyDTO> toCapCQQLDTOList(Iterable<CapCoQuanQuanLy> CapCoQuanQuanLyIterable);

    CapCoQuanQuanLy toCapCoQuanQuanLy(CapCoQuanQuanLyDTO CapCoQuanQuanLyDTO);
}
