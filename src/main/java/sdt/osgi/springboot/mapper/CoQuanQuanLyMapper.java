package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.CoQuanQuanLyDTO;
import sdt.osgi.springboot.dto.VaiTroDTO;
import sdt.osgi.springboot.model.CoQuanQuanLy;
import sdt.osgi.springboot.model.VaiTro;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CoQuanQuanLyMapper {
    CoQuanQuanLyDTO toCoQuanQuanLyDTO (CoQuanQuanLy coQuanQuanLy);
    List<CoQuanQuanLyDTO> toCQQLDTOList(Iterable<CoQuanQuanLy> coQuanQuanLyIterable);
    CoQuanQuanLy toCoQuanQuanLy (CoQuanQuanLyDTO coQuanQuanLyDTO);
}
