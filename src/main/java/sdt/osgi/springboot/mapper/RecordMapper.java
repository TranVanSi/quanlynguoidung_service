package sdt.osgi.springboot.mapper;

import sdt.osgi.springboot.dto.RecordDTO;
import sdt.osgi.springboot.model.Record;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RecordMapper {
    Record recordDTOToRecord(RecordDTO recordDTO);
}