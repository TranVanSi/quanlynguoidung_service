package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.VaiTro2TaiNguyenDTO;
import sdt.osgi.springboot.dto.VaiTroDTO;
import sdt.osgi.springboot.model.VaiTro;

import java.util.List;

@Mapper(componentModel = "spring")
public interface VaiTroMapper {
    VaiTro vaiTroDTOToVaiTro(VaiTroDTO vaiTroDTO);
    VaiTroDTO vaiTroToVaiTroDTO(VaiTro vaiTro);
    List<VaiTroDTO> vaiTrosToVaiTroDTOs(List<VaiTro> vaiTros);
    List<VaiTroDTO> toVaiTroDTOList(Iterable<VaiTro> vaiTros);

    VaiTro2TaiNguyenDTO toVaiTro2TaiNguyen (VaiTro vaiTro);
}
