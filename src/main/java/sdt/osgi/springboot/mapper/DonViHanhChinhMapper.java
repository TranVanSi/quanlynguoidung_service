package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DonViHanhChinhDTO;
import sdt.osgi.springboot.model.DonViHanhChinh;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DonViHanhChinhMapper {
    @Mapping(source = "donViHanhChinhDTO.capDonViHanhChinhId", target = "capDonViHanhChinh.id")
    DonViHanhChinh toDonViHanhChinh(DonViHanhChinhDTO donViHanhChinhDTO);

    @Mapping(source = "donViHanhChinh.capDonViHanhChinh.ten", target = "tenCapDonViHanhChinh")
    @Mapping(source = "donViHanhChinh.capDonViHanhChinh.id", target = "capDonViHanhChinhId")
    DonViHanhChinhDTO toDonViHanhChinhDTO(DonViHanhChinh donViHanhChinh);

    List<DonViHanhChinhDTO> toDonViHanhChinhDTOList(List<DonViHanhChinh> donViHanhChinhs);

    List<DonViHanhChinh> toDonViHanhChinhList(List<DonViHanhChinhDTO> donViHanhChinhDTOS);
}
