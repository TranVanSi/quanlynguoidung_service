package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.osgi.springboot.model.ChucVu2CoQuan2VaiTro;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ChucVu2CoQuan2VaiTroMapper {
    ChucVu2CoQuan2VaiTro chucVu2CoQuan2VaiTroDTOToChucVu2CoQuan2VaiTro(ChucVu2CoQuan2VaiTroDTO chucVu2CoQuan2VaiTroDTO);
    ChucVu2CoQuan2VaiTroDTO chucVu2CoQuan2VaiTroToChucVu2CoQuan2VaiTroDTO(ChucVu2CoQuan2VaiTro chucVu2CoQuan2VaiTro);
    List<ChucVu2CoQuan2VaiTroDTO> toChucVu2CoQuan2VaiTroDTOList (List<ChucVu2CoQuan2VaiTro> chucVu2CoQuan2VaiTroList);
}
