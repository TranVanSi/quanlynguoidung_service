package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.NguoiDung2VaiTroDTO;
import sdt.osgi.springboot.model.NguoiDung2VaiTro;

import java.util.List;

@Mapper(componentModel = "spring")
public interface NguoiDung2VaiTroMapper {
    NguoiDung2VaiTro nguoiDung2VaiTroDTOToNguoiDung2VaiTro(NguoiDung2VaiTroDTO nguoiDung2VaiTroDTO);
    NguoiDung2VaiTroDTO nguoiDung2VaiTroToNguoiDung2VaiTroDTO(NguoiDung2VaiTro nguoiDung2VaiTro);
    List<NguoiDung2VaiTroDTO> nguoiDung2VaiTrosToNguoiDung2VaiTroDTOs(List<NguoiDung2VaiTro> nguoiDung2VaiTros);
    List<NguoiDung2VaiTroDTO> toNguoiDung2VaiTroDTOList(Iterable<NguoiDung2VaiTro> nguoiDung2VaiTros);
    List<NguoiDung2VaiTro> toNguoiDung2VaiTroList(List<NguoiDung2VaiTroDTO> nguoiDung2VaiTros);
}
