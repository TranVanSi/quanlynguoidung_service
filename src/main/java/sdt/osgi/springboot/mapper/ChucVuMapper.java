package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.ChucVuDTO;
import sdt.osgi.springboot.model.ChucVu;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ChucVuMapper {
    ChucVu chucVuDTOToChucVu(ChucVuDTO chucVuDTO);
    ChucVuDTO chucVuToChucVuDTO(ChucVu chucVu);
    List<ChucVuDTO> chucVusToChucVuDTOs (List<ChucVu> chucVus);
    List<ChucVuDTO> toChucVuDTOList(Iterable<ChucVu> chucVus);

    List<ChucVu> toChucVuList(List<ChucVuDTO> chucVuDTOList);
}
