package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.TaiNguyenDTO;
import sdt.osgi.springboot.model.TaiNguyen;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaiNguyenMapper {
    TaiNguyen taiNguyenDTOToTaiNguyen(TaiNguyenDTO taiNguyenDTO);
    TaiNguyenDTO taiNguyenToTaiNguyenDTO(TaiNguyen taiNguyen);
    List<TaiNguyenDTO> taiNguyensTotaiNguyenDTOs(List<TaiNguyen> taiNguyens);
    List<TaiNguyenDTO> toTaiNguyenDTOList(Iterable<TaiNguyen> taiNguyens);
}
