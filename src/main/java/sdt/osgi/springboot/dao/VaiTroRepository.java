package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.VaiTro;

import java.util.List;

@Repository
public interface VaiTroRepository extends PagingAndSortingRepository<VaiTro, Long> {
    VaiTro findFirstByMa(String ma);
    VaiTro findFirstByTen(String ten);
    List<VaiTro> findTop1000By();
    Page<VaiTro> findByMaContainingOrTenContaining(String keyword, Pageable pageable);
    Page<VaiTro> findAllByOrderByNgaySuaDesc(Pageable pageable);
    List<VaiTro> findByIdNotIn(List<Long> ids);
    List<VaiTro> findByIdIn(List<Long> ids);

    List<VaiTro> findAllByTenContainingOrMaContaining(String tenVaiTro, String maVaiTro);

    List<VaiTro> findByIdNotInAndTrangThai(List<Long> ids, Integer trangThai);
    List<VaiTro> findByTrangThai(Integer trangThai);

    @Query(value = "select distinct tai_nguyen_id from tai_nguyen2vai_tro  t2v join chuc_vu2co_quan2vai_tro c on c.vai_tro_id = t2v.vai_tro_id where c.co_quan_quan_ly_id = ?1 and c.chuc_vu_id = ?2 ;",
            nativeQuery = true)
    List<Long> findAllVaiTro(long coQuanId, long chucVuId);
}
