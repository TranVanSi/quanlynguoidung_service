package sdt.osgi.springboot.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DonViHanhChinh;

import java.util.List;

@Repository
public interface DonViHanhChinhRepository extends PagingAndSortingRepository<DonViHanhChinh, Long> {
    List<DonViHanhChinh> findByCapDonViHanhChinh_IdAndDaXoa(long capId, int daXoa);
}
