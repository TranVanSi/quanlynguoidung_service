package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.TaiNguyen;

import java.util.List;

@Repository
public interface TaiNguyenRepository extends PagingAndSortingRepository<TaiNguyen, Long> {
    TaiNguyen findFirstByMa(String ma);

    TaiNguyen findFirstByTen(String ten);

    List<TaiNguyen> findTop1000By();

    Page<TaiNguyen> findByMaContainingOrTenContaining(String keyword, Pageable pageable);

    List<TaiNguyen> findByIdNotIn(List<Long> ids);

    List<TaiNguyen> findByIdIn(List<Long> ids);

    Page<TaiNguyen> findAllByOrderByNgaySuaDesc(Pageable pageable);

    List<TaiNguyen> findAllByTenContainingOrMaContaining(String tenTaiNguyen, String maTaiNguyen);

    List<TaiNguyen> findByIdNotInAndTrangThai(List<Long> ids, Integer trangThai);

    List<TaiNguyen> findByTrangThai(Integer trangThai);

    @Query(value = "select distinct tai_nguyen_id from tai_nguyen2vai_tro  t2v join chuc_vu2co_quan2vai_tro c on c.vai_tro_id = t2v.vai_tro_id where c.co_quan_quan_ly_id = ?1 and c.chuc_vu_id = ?2 ;",
            nativeQuery = true)
    List<Long> findAllTaiNguyenIdByCoQuanIdAndChucVuId(long coQuanId, long chucVuId);
}
