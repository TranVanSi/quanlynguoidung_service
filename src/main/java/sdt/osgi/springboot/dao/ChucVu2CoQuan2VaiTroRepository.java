package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.ChucVu2CoQuan2VaiTro;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface ChucVu2CoQuan2VaiTroRepository extends DataTablesRepository<ChucVu2CoQuan2VaiTro, Long> {
   List<ChucVu2CoQuan2VaiTro> findByCoQuanQuanLyIdAndChucVu_IdIn(long coQuanId,List<Long> chucVuIds);
   List<ChucVu2CoQuan2VaiTro> findByCoQuanQuanLyId(long coQuanId);

   @Query(value = "select DISTINCT c.chuc_vu_id from chuc_vu2co_quan2vai_tro c where co_quan_quan_ly_id = ?1  and chuc_vu_id in (select id from chuc_vu cv where cv.ma like %?2% or cv.ten like %?2%)",
           nativeQuery = true)
   Page<BigInteger> findChucVuByCoQuanId(long coQuanId, String search, Pageable pageable);
   List<ChucVu2CoQuan2VaiTro> findByCoQuanQuanLyIdAndChucVu_Id(long coQuanId, long chucVuId);
   void deleteByCoQuanQuanLyIdAndChucVu_Id(long coQuanId, long chucVuId);
}
