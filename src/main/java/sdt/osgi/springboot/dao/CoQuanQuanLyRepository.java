package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.CoQuanQuanLy;
import sdt.osgi.springboot.model.CongChuc;
import sdt.osgi.springboot.model.VaiTro;

import java.util.List;

@Repository
public interface CoQuanQuanLyRepository extends PagingAndSortingRepository<CoQuanQuanLy, Long>, DataTablesRepository<CoQuanQuanLy, Long> {
    CoQuanQuanLy findFirstByMaAndDaKhoaAndTrangThai(String ma, int daKhoa, int trangThai);
    CoQuanQuanLy findFirstByTenAndDaKhoa(String ten, int daKhoa);
    List<CoQuanQuanLy> findTop1000By();
    Page<CoQuanQuanLy> findAllByOrderByNgaySuaDesc(Pageable pageable);
    List<CoQuanQuanLy> findByIdNotIn(List<Long> ids);
    List<CoQuanQuanLy> findByIdInAndTrangThai(List<Long> ids, int trangThai);
    List<CoQuanQuanLy> findByChaIdAndTrangThai(long parentId, int trangThai);
    List<CoQuanQuanLy> findByChaIdAndCapCoQuanQuanLyIdInAndTrangThai(long parentId, List<Long> listCap, int trangThai);
    List<CoQuanQuanLy> findByChaIdInAndCapCoQuanQuanLyIdInAndTrangThai(List<Long> parentId, List<Long> listCap, int trangThai);
    List<CoQuanQuanLy> findByDaKhoaAndTrangThai(int daKhoa, int trangThai);

    Page<CoQuanQuanLy> findAllByDaKhoa(int daKhoa, Pageable pageable);

    Page<CoQuanQuanLy> findByChaIdAndDaKhoa(long chaId, int daKhoa, Pageable pageable);

    List<CoQuanQuanLy> findAllByTenContainingOrMaContainingAndDaKhoaAndTrangThai (String tenCoQuan, String maCoQuan, int daKhoa, int trangThai);

    List<CoQuanQuanLy> findAllByTenContainingOrMaContainingAndChaIdAndDaKhoaAndTrangThai(String tenCoQuan, String maCoQuan, long parentId, int daKhoa, int trangThai);

    Page<CoQuanQuanLy> findByMaContainingOrTenContainingAndDaKhoa(String ma, String hoVaTen, int daKhoa, Pageable pageable);

    Page<CoQuanQuanLy> findByMaContainingAndChaIdOrTenContainingAndChaIdAndDaKhoa(String ma, long chaId1, String hoVaTen, long chaId, int daKhoa, Pageable pageable);

    Page<CoQuanQuanLy> findByDiaChiContainingAndChaIdAndDaKhoa(String diaChi, long chaId, int daKhoa, Pageable pageable);
    Page<CoQuanQuanLy> findByDiaChiContainingAndDaKhoa(String diaChi, int daKhoa, Pageable pageable);

    List<CoQuanQuanLy> findAllByChaIdIn(List<Long> coQuanIds);
    List<CoQuanQuanLy> findAllByIdIn(List<Long> coQuanIds);
    List<CoQuanQuanLy> findByCapCoQuanQuanLy_Id(Long id);
    CoQuanQuanLy findFirstByIdAndTrangThai(Long id, int daKhoa);
}
