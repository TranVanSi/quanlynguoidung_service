package sdt.osgi.springboot.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.TaiKhoanNguoiDung;

import java.util.List;

@Repository
public interface TaiKhoanNguoiDungRepository extends PagingAndSortingRepository<TaiKhoanNguoiDung, Long> {
    List<TaiKhoanNguoiDung> findTop1000By();
    TaiKhoanNguoiDung findFirstByEmail(String email);
    TaiKhoanNguoiDung findByUserId(Long userId);
}
