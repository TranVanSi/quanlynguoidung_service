package sdt.osgi.springboot.dao;

import sdt.osgi.springboot.model.Record;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordsRepository extends CrudRepository<Record, Long> {

}
