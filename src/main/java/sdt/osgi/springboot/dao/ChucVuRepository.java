package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.ChucVu;

import java.util.List;

@Repository
public interface ChucVuRepository extends DataTablesRepository<ChucVu, Long> {
    ChucVu findFirstByMaAndDaXoa(String ma, int daXoa);
    ChucVu findFirstByTen(String ten);
    List<ChucVu> findTop1000ByDaXoa(int daXoa);
    List<ChucVu> findTop1000ByIdNotIn(List<Long> ids);

    List<ChucVu> findByDaXoa(int daXoa);

    Page<ChucVu> findByMaContainingOrTenContaining(String keyword, Pageable pageable);

    List<ChucVu> findAllByTenContainingOrMaContainingAndDaXoa(String tenChucVu, String maChucVu, int daXoa);

    List<ChucVu> findByTenContaining(String ten);
}
