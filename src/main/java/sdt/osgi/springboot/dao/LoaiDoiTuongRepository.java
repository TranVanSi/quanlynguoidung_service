package sdt.osgi.springboot.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.LoaiDoiTuong;

import java.util.List;

@Repository
public interface LoaiDoiTuongRepository extends PagingAndSortingRepository<LoaiDoiTuong, Long> {
    List<LoaiDoiTuong> findTop1000By();
}
