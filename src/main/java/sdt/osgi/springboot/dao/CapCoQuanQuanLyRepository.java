package sdt.osgi.springboot.dao;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.CapCoQuanQuanLy;

@Repository
public interface CapCoQuanQuanLyRepository extends PagingAndSortingRepository<CapCoQuanQuanLy, Long>, DataTablesRepository<CapCoQuanQuanLy, Long> {
}
