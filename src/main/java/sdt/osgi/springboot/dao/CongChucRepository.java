package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.CoQuanQuanLy;
import sdt.osgi.springboot.model.CongChuc;

import java.util.List;

@Repository
public interface CongChucRepository extends JpaRepository<CongChuc, Long>, DataTablesRepository<CongChuc, Long>, PagingAndSortingRepository<CongChuc, Long> {

    Page<CongChuc> findByMaContainingOrHoVaTenContainingOrEmailContaining(String keyword, Pageable pageable);

    CongChuc findFirstByMaAndDaXoa(String ma, int daXoa);

    CongChuc findFirstByTaiKhoanNguoiDungIdAndDaXoa(long taiKhoanNguoiDungId, int daXoa);

    List<CongChuc> findTop1000By();

    List<CongChuc> findByCoQuanQuanLyId(long coQuanQuanLyId);

    List<CongChuc> findByDaXoa(int daXoa);

    Page<CongChuc> findByDaXoa(int daXoa, Pageable pageable);

    Page<CongChuc> findByCoQuanQuanLy_IdAndDaXoa(long coQuanQuanLyId, int daXoa, Pageable pageable);

    Page<CongChuc> findByMaContainingAndDaXoaOrHoVaTenContainingAndDaXoaOrEmailContainingAndDaXoa(String ma, int daXoa1, String hoVaTen, int daXoa, String email, int daXoa2, Pageable pageable);

    Page<CongChuc> findByCoQuanQuanLy_IdAndMaContainingAndDaXoaOrCoQuanQuanLy_IdAndHoVaTenContainingAndDaXoaOrCoQuanQuanLy_IdAndEmailContainingAndDaXoa(long coQuanQuanLyId1, String ma, int daXoa1, long coQuanQuanLyId, String hoVaTen, int daXoa, long coQuanQuanLyId2, String email, int daXoa2, Pageable pageable);

    @Query(value = "select * from cong_chuc cc WHERE cc.co_quan_quan_ly_id = ?1 and chuc_vu_id in (select chuc_vu_id from chuc_vu2co_quan2vai_tro c join tai_nguyen2vai_tro t2v on c.vai_tro_id = t2v.vai_tro_id WHERE c.co_quan_quan_ly_id = ?1 and t2v.tai_nguyen_id = ?2);",
            nativeQuery = true)
    List<CongChuc> findAllByCoQuanAndTaiNguyen(long coQuanId, long taiNguyenId);

    CongChuc findByTaiKhoanNguoiDung_Email(String email);

    List<CongChuc> findByChucVu_IdAndDaXoa(long chucVuId, int daXoa);
    List<CongChuc> findByCoQuanQuanLyIdIn(List<Long> coQuanQuanLyIdIn);
    List<CongChuc> findAllByIdIn(List<Long> congChucIds);
}
