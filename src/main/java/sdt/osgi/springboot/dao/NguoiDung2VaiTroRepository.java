package sdt.osgi.springboot.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.NguoiDung2VaiTro;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface NguoiDung2VaiTroRepository extends PagingAndSortingRepository<NguoiDung2VaiTro, Long> {

    List<NguoiDung2VaiTro> findTop1000By();

    List<NguoiDung2VaiTro> findByIdIn(List<Long> ids);

    List<NguoiDung2VaiTro> findAllByCongChucId(Long congChucId);
    void deleteByCongChuc_IdAndVaiTro_Id(long congChucId, long vaiTroId);
}
