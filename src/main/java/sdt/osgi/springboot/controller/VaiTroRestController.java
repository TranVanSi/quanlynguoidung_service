package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.VaiTroDTO;
import sdt.osgi.springboot.mapper.VaiTroMapper;
import sdt.osgi.springboot.model.VaiTro;
import sdt.osgi.springboot.service.VaiTroService;
import sdt.osgi.springboot.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/vaitro")
public class VaiTroRestController {

	private final VaiTroService vaiTroService;
	private final VaiTroMapper vaiTroMapper;

	@GetMapping("/{vaiTroId}")
	public VaiTroDTO getVaiTro(@PathVariable Long vaiTroId) {

		return vaiTroService.getVaiTro(vaiTroId);
	}

	@GetMapping
	public Page<VaiTroDTO> getVaiTros(@RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
									@RequestParam(value = "size", defaultValue = "20") int size,
								   @RequestParam(value = "keyword", defaultValue = "") String keyword) throws Exception{

		if (keyword.trim().length() > 0) {

			return vaiTroService.findByKeyword(StringUtils.decode(keyword), pageIndex, size);
		}

		return vaiTroService.findAll(pageIndex, size);
	}

	@GetMapping("/listAllVaiTro")
	public List<VaiTroDTO> getListVaiTro() {

		return vaiTroService.getListVaiTro();
	}

	@GetMapping("/listAllVaiTroBySearch")
	public List<VaiTroDTO> getListVaiTroBySearch(@RequestParam(value = "keyword", defaultValue = "") String keyword) throws Exception {
		if (keyword.trim().length() > 0) {
			keyword = StringUtils.decode(keyword);
		}
		return vaiTroService.getListVaiTroBySearch(keyword);
	}

	@GetMapping("/findTop1000")
	public List<VaiTro> findTop1000(){

		return vaiTroService.findTop1000();
	}

	@GetMapping("/findByMa")
	public VaiTroDTO findFirstByMa(@RequestParam(value = "ma", defaultValue = "") String ma){

		return vaiTroService.findFirstByMa(ma);
	}

	@GetMapping("/findByTen")
	public VaiTroDTO findFirstByTen(@RequestParam(value = "ten", defaultValue = "") String ten) throws Exception{

		return vaiTroService.findFirstByTen(StringUtils.decode(ten));
	}

	@PutMapping("/update")
	public VaiTroDTO updateVaiTro(@RequestBody VaiTroDTO vaiTroDTO) {

		vaiTroService.update(vaiTroDTO);

		return vaiTroDTO;
	}

    @PostMapping("/create")
    public VaiTroDTO addVaiTro(@RequestBody VaiTroDTO vaiTroDTO) {

        vaiTroService.add(vaiTroDTO);

        return vaiTroDTO;
    }

	@DeleteMapping("/delete/{vaiTroId}")
	public boolean deleteVaiTro(@PathVariable Long vaiTroId) {

		vaiTroService.delete(vaiTroId);

		return true;

	}

	@GetMapping("/findByIdIsNotIn")
	public List<VaiTroDTO> findByIdIsNotIn(@RequestParam(value = "vaiTroIds", defaultValue = "") String idsStr){

		if (idsStr.length() > 0) {
			List<Long> ids = StringUtils.convertStringToLongs(idsStr);
			return vaiTroService.findByVaiTroIsNotInAndTrangThai(ids, 1);
		}

		return vaiTroMapper.vaiTrosToVaiTroDTOs(vaiTroService.findByTrangThai(1));
	}

	@GetMapping("/findByIdIn")
	public List<VaiTroDTO> findByIdIn(@RequestParam(value = "vaiTroIds", defaultValue = "") String idsStr){

		if (idsStr.length() > 0) {
			List<Long> ids = StringUtils.convertStringToLongs(idsStr);

			return vaiTroService.findByVaiTroIn(ids);
		}

		return new ArrayList<>();
	}
}
