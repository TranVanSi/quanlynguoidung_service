package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.CoQuanQuanLyDTO;
import sdt.osgi.springboot.dto.CongChucDTO;
import sdt.osgi.springboot.dto.VaiTro2TaiNguyenDTO;
import sdt.osgi.springboot.model.CongChuc;
import sdt.osgi.springboot.service.CongChucService;
import sdt.osgi.springboot.util.StringUtils;

import javax.validation.Valid;
import java.net.URLDecoder;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/congchuc")
public class CongChucRestController {

    private final CongChucService congChucService;

    @GetMapping("/findByCoQuanQuanLyId")
    public List<CongChucDTO> getListCongChucByCoQuanQuanLyId(@RequestParam Long coQuanQuanLyId) {

        return congChucService.getListCongChucByCoQuanQuanLyId(coQuanQuanLyId);
    }

    @GetMapping("/getAllCongChuc")
    public List<CongChucDTO> getAllCongChuc() {

        return congChucService.getListCongChuc();
    }

    @GetMapping("/{congChucId}")
    public CongChucDTO getCongChuc(@PathVariable Long congChucId) {

        return congChucService.getCongChuc(congChucId);
    }

    @GetMapping
    public Page<CongChuc> getCongChucs(@RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                                       @RequestParam(value = "size", defaultValue = "20") int size,
                                       @RequestParam(value = "search", defaultValue = "") String keyword) throws Exception {

        if (keyword.trim().length() > 0) {
            keyword = URLDecoder.decode(keyword, "UTF-8");
            return congChucService.findByKeyword(keyword, pageIndex, size);
        }

        return congChucService.findAll(pageIndex, size);
    }

    @GetMapping("/findTop1000")
    public List<CongChuc> findTop1000() {

        return congChucService.findTop1000();
    }

    @PutMapping("/update")
    public void updateCongChuc(@RequestBody CongChucDTO congChucDTO) {

        congChucService.update(congChucDTO);
    }

    @PostMapping("/create")
    public CongChucDTO addCongChuc(@RequestBody CongChucDTO congChucDTO) {

        congChucService.add(congChucDTO);

        return congChucDTO;
    }

    @DeleteMapping("/delete/{congChucId}")
    public boolean deleteCongChuc(@PathVariable Long congChucId) {

        congChucService.delete(congChucId);

        return true;

    }

    @PostMapping
    public DataTablesOutput<CongChuc> getCongChucDataTable(@Valid @RequestBody DataTablesInput input) {

        return congChucService.findAll(input);
    }

    @GetMapping("/findByMa")
    public CongChucDTO findFirstByMa(@RequestParam(value = "ma", defaultValue = "") String ma) throws Exception {

        return congChucService.findFirstByMa(StringUtils.decode(ma));
    }

    @GetMapping("/findByTaiKhoanNguoiDungId")
    public CongChucDTO findByTaiKhoanNguoiDungId(@RequestParam(value = "taiKhoanNguoiDungId", defaultValue = "") long taiKhoanNguoiDungId) throws Exception {

        return congChucService.findByTaiKhoanNguoiDungId(taiKhoanNguoiDungId);
    }

    @GetMapping("/findByEmailNguoiDung")
    public Long findByEmailNguoiDung(@RequestParam(value = "email", defaultValue = "") String email) throws Exception {
        return congChucService.findByEmailNguoiDung(email);
    }

    @GetMapping("/findToPaging")
    public Page<CongChuc> findToPaging(@RequestParam(value = "coQuanQuanLyId", defaultValue = "0") long coQuanQuanLyId,
                                       @RequestParam(value = "keyword", defaultValue = "") String keyword,
                                       @RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                                       @RequestParam(value = "size", defaultValue = "20") int size) throws Exception {

        return congChucService.findToPaging(coQuanQuanLyId, StringUtils.decode(keyword), pageIndex, size);
    }

    @GetMapping("/getCongChucByCoQuanAndTaiNguyenId")
    public List<CongChucDTO> getCongChucByCoQuanAndTaiNguyenId(@RequestParam(value = "coQuanQuanLyId", defaultValue = "0") long coQuanQuanLyId,
                                                               @RequestParam(value = "taiNguyenId", defaultValue = "0") long taiNguyenId) {
          return congChucService.getCongChucByCoQuanAndTaiNguyenId(coQuanQuanLyId, taiNguyenId);
    }

    @GetMapping(value = "/getPermission/{congChucId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<VaiTro2TaiNguyenDTO> getPermission(@PathVariable Long congChucId) {

        return congChucService.getPermission(congChucId);
    }

    @GetMapping(value = "/findByChucVuId/{chucVuId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CongChucDTO> getListCongChucByChucVuId(@PathVariable Long chucVuId) {

        return congChucService.getListCongChucByChucVuId(chucVuId);
    }

    @GetMapping("/listCongChucByCQQLIds")
    public List<CongChucDTO> listCoQuanQuanLyByParentIdAndCap(@RequestParam(value = "coQuanQuanLyIds", defaultValue = "0") String coQuanQuanLyIds) {

        return congChucService.getListCongChucByCQQLIds(coQuanQuanLyIds);
    }

    @GetMapping("/findAllCongChucByIdIn")
    public List<CongChucDTO> findAllByIdIn(@RequestParam(value = "ids", defaultValue = "0") String ids) {

        return congChucService.findAllByIdIn(ids);
    }
}