package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.CapCoQuanQuanLyDTO;
import sdt.osgi.springboot.dto.CoQuanQuanLyDTO;
import sdt.osgi.springboot.mapper.CoQuanQuanLyMapper;
import sdt.osgi.springboot.model.CoQuanQuanLy;
import sdt.osgi.springboot.service.CoQuanQuanLyService;
import sdt.osgi.springboot.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/coquanquanly")
public class CoQuanQuanLyRestController {

    private final CoQuanQuanLyService coQuanQuanLyService;
    private final CoQuanQuanLyMapper coQuanQuanLyMapper;

    @PutMapping("/update")
    public CoQuanQuanLyDTO updateCoQuanQuanLy(@RequestBody CoQuanQuanLyDTO coQuanQuanLy) {

        coQuanQuanLyService.update(coQuanQuanLy);

        return coQuanQuanLy;
    }

    @PostMapping("/create")
    public CoQuanQuanLyDTO addVaiTro(@RequestBody CoQuanQuanLyDTO coQuanQuanLyDTO) {


        coQuanQuanLyService.add(coQuanQuanLyDTO);

        return coQuanQuanLyDTO;
    }

    @DeleteMapping("/delete/{coQuanQuanLyId}")
    public boolean deleteCoQuanQuanLy(@PathVariable Long coQuanQuanLyId) {

        coQuanQuanLyService.delete(coQuanQuanLyId);

        return true;

    }

    @GetMapping("/findById")
    public CoQuanQuanLyDTO getCoQuanQuanLy(@RequestParam(value = "coQuanQuanLyId", defaultValue = "0") long coQuanQuanLyId) {

        return coQuanQuanLyService.getCoQuanQuanLy(coQuanQuanLyId);
    }

    @GetMapping("/listAllCoQuanQuanLy")
    public List<CoQuanQuanLyDTO> getListCoQuanQuanLy() {

        return coQuanQuanLyService.getListCoQuanQuanLy();
    }

    @GetMapping("/listAllCoQuanQuanLyBySearch")
    public List<CoQuanQuanLyDTO> getListCoQuanQuanLyBySearch(@RequestParam(value = "keyword", defaultValue = "") String keyword) throws UnsupportedEncodingException {
        if (keyword.trim().length() > 0) {
            keyword = URLDecoder.decode(keyword, "UTF-8");
        }
        return coQuanQuanLyService.getListCoQuanQuanLyBySearch(keyword);
    }

    @GetMapping("/listCoQuanQuanLyByParentId")
    public List<CoQuanQuanLyDTO> getListCoQuanQuanLyByParentId(@RequestParam(value = "parentId", defaultValue = "0") long parentId) {

        return coQuanQuanLyService.getListCoQuanQuanLyByParentId(parentId);
    }

    @GetMapping("/listCoQuanQuanLyByParentIdBySearch")
    public List<CoQuanQuanLyDTO> getListCoQuanQuanLyByParentIdBySearch(@RequestParam(value = "parentId", defaultValue = "0") long parentId,
                                                                       @RequestParam(value = "keyword", defaultValue = "") String keyword) {

        return coQuanQuanLyService.getListCoQuanQuanLyByParentIdBySearch(parentId, keyword);
    }

    @GetMapping("/getListCoQuanQuanLyByDScoQuanId/{dsCoQuanQuanLyId}")
    public List<CoQuanQuanLyDTO> getListCoQuanQuanLyByParentId(@PathVariable String dsCoQuanQuanLyId) {

        return coQuanQuanLyService.getListCoQuanQuanLyByDsCoQuanQuanLyId(dsCoQuanQuanLyId);
    }

    @GetMapping("/findByMa")
    public CoQuanQuanLyDTO findFirstByMa(@RequestParam(value = "ma", defaultValue = "") String ma) {

        return coQuanQuanLyService.findFirstByMa(ma);
    }

    @GetMapping("/getListCoQuanQuanLyAndPaginate")
    public Page<CoQuanQuanLy> getListCoQuanQuanLyAndPaginate(@RequestParam(value = "keyword", defaultValue = "") String keyword,
                                                             @RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                                                             @RequestParam(value = "size", defaultValue = "10") int size,
                                                             @RequestParam(value = "chaId", defaultValue = "-1") long chaId) throws Exception {
        return coQuanQuanLyService.getListCoQuanQuanLyAndPaginate(StringUtils.decode(keyword), pageIndex, size, chaId);
    }

    @GetMapping("/getListCoQuanQuanLyAndPaginateByDonVi")
    public Page<CoQuanQuanLy> getListCoQuanQuanLyAndPaginateByDonVi(@RequestParam(value = "keyword", defaultValue = "") String keyword,
                                                             @RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                                                             @RequestParam(value = "size", defaultValue = "10") int size,
                                                             @RequestParam(value = "chaId", defaultValue = "-1") long chaId) throws Exception {
        return coQuanQuanLyService.getListCoQuanQuanLyAndPaginateByDonVi(StringUtils.decode(keyword), pageIndex, size, chaId);
    }


    @GetMapping("/listCoQuanQuanLyByParentIdAndCap")
    public List<CoQuanQuanLyDTO> listCoQuanQuanLyByParentIdAndCap(@RequestParam(value = "parentId", defaultValue = "0") long parentId,
                                                                  @RequestParam(value = "capCoQuanQuanLyIds", defaultValue = "0") String capCoQuanQuanLyIds) {

        return coQuanQuanLyService.getListCoQuanQuanLyByParentIdAndCap(parentId, capCoQuanQuanLyIds);
    }

    @GetMapping("/listCoQuanQuanLyByParentIdsAndCaps")
    public List<CoQuanQuanLyDTO> listCoQuanQuanLyByParentIdsAndCaps(@RequestParam(value = "parentIds", defaultValue = "0") String parentIds,
                                                                    @RequestParam(value = "capCoQuanQuanLyIds", defaultValue = "0") String capCoQuanQuanLyIds) {

        return coQuanQuanLyService.findByChaIdInAndCapCoQuanQuanLyIdIn(parentIds, capCoQuanQuanLyIds);
    }

    @GetMapping("/listCapCoQuanQuanLy")
    public List<CapCoQuanQuanLyDTO> listCapCoQuanQuanLy() {
        return coQuanQuanLyService.getAllCapCoQuanQuanLy();
    }

    @PutMapping("/khoa")
    public boolean khoaCoQuanQuanLyy(@RequestParam(value = "coQuanQuanLyId", defaultValue = "0") long coQuanQuanLyId,
                                     @RequestParam(value = "trangThai", defaultValue = "0") int trangThai) {

        coQuanQuanLyService.khoaCoQuanQuanLy(coQuanQuanLyId, trangThai);
        return true;
    }

    @GetMapping("/findAllByChaIdIn")
    public List<CoQuanQuanLyDTO> findAllByChaIdIn(@RequestParam(value = "parentIds", defaultValue = "0") String parentIds) {

        return coQuanQuanLyService.findAllByChaIdIn(parentIds);
    }

    @GetMapping("/findAllByIdIn")
        public List<CoQuanQuanLyDTO> findAllByIdIn(@RequestParam(value = "ids", defaultValue = "0") String ids) {

        return coQuanQuanLyService.findAllByIdIn(ids);
    }

    @GetMapping("/findByCapId")
    public List<CoQuanQuanLyDTO> findByCapId(@RequestParam(value = "capId", defaultValue = "0") Long id) {

        return coQuanQuanLyService.findByCapId(id);
    }
}
