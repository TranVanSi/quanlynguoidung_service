package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.LoaiDoiTuongDTO;
import sdt.osgi.springboot.model.LoaiDoiTuong;
import sdt.osgi.springboot.service.LoaiDoiTuongService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/loaidoituong")
public class LoaiDoiTuongRestController {

	private final LoaiDoiTuongService loaiDoiTuongService;

	@GetMapping("/findTop1000")
	public List<LoaiDoiTuong> findTop1000(){

		return loaiDoiTuongService.findTop1000();
	}

	@PutMapping("/update")
	public LoaiDoiTuongDTO updateLoaiDoiTuong(@RequestBody LoaiDoiTuongDTO loaiDoiTuongDTO) {

		loaiDoiTuongService.update(loaiDoiTuongDTO);

		return loaiDoiTuongDTO;
	}

    @PostMapping("/create")
    public LoaiDoiTuongDTO addLoaiDoiTuong(@RequestBody LoaiDoiTuongDTO loaiDoiTuongDTO) {

        loaiDoiTuongService.add(loaiDoiTuongDTO);

        return loaiDoiTuongDTO;
    }
}