package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.TaiNguyenDTO;
import sdt.osgi.springboot.mapper.TaiNguyenMapper;
import sdt.osgi.springboot.model.TaiNguyen;
import sdt.osgi.springboot.service.TaiNguyenService;
import sdt.osgi.springboot.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/tainguyen")
public class TaiNguyenRestController {

    private final TaiNguyenService taiNguyenService;
    private final TaiNguyenMapper taiNguyenMapper;

    @GetMapping("/{taiNguyenId}")
    public TaiNguyenDTO getTaiNguyen(@PathVariable Long taiNguyenId) {

        return taiNguyenService.getTaiNguyen(taiNguyenId);
    }

    @GetMapping("/listAllTaiNguyen")
    public List<TaiNguyenDTO> getListTaiNguyen() {

        return taiNguyenService.getListTaiNguyen();
    }

    @GetMapping("/listAllTaiNguyenBySearch")
    public List<TaiNguyenDTO> getListTaiNguyenBySearch(@RequestParam(value = "keyword", defaultValue = "") String keyword) throws Exception {
        if (keyword.trim().length() > 0) {
            keyword = StringUtils.decode(keyword);
        }

        return taiNguyenService.getListTaiNguyenBySearch(keyword);
    }

    @GetMapping
    public Page<TaiNguyen> getTaiNguyens(@RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                                         @RequestParam(value = "size", defaultValue = "20") int size,
                                         @RequestParam(value = "keyword", defaultValue = "") String keyword) throws Exception {

        if (keyword.trim().length() > 0) {
            return taiNguyenService.findByKeyword(StringUtils.decode(keyword), pageIndex, size);
        }

        return taiNguyenService.findAll(pageIndex, size);
    }

    @GetMapping("/findTop1000")
    public List<TaiNguyen> findTop1000() {

        return taiNguyenService.findTop1000();
    }

    @PutMapping("/update")
    public TaiNguyenDTO updateTaiNguyen(@RequestBody TaiNguyenDTO taiNguyenDTO) {

        taiNguyenService.update(taiNguyenDTO);

        return taiNguyenDTO;
    }

    @PostMapping("/create")
    public TaiNguyenDTO addTaiNguyen(@RequestBody TaiNguyenDTO taiNguyenDTO) {

        taiNguyenService.add(taiNguyenDTO);

        return taiNguyenDTO;
    }

    @DeleteMapping("/delete/{taiNguyenId}")
    public boolean deleteTaiNguyen(@PathVariable Long taiNguyenId) {

        taiNguyenService.delete(taiNguyenId);

        return true;

    }

    @GetMapping("/findByMa")
    public TaiNguyenDTO findFirstByMa(@RequestParam(value = "ma", defaultValue = "") String ma) throws Exception {

        return taiNguyenService.findFirstByMa(StringUtils.decode(ma));
    }

    @GetMapping("/findByTen")
    public TaiNguyenDTO findFirstByTen(@RequestParam(value = "ten", defaultValue = "") String ten) throws Exception {

        return taiNguyenService.findFirstByTen(StringUtils.decode(ten));
    }

    @GetMapping("/findByIdIsNotIn")
    public List<TaiNguyenDTO> findByIdIsNotIn(@RequestParam(value = "taiNguyenIds", defaultValue = "") String idsStr) {

        if (idsStr.length() > 0) {
            List<Long> ids = StringUtils.convertStringToLongs(idsStr);
            return taiNguyenService.findByTaiNguyenIsNotInAndTrangThai(ids, 1);
        }
        return taiNguyenMapper.taiNguyensTotaiNguyenDTOs(taiNguyenService.findByTrangThai(1));
    }

    @GetMapping("/findByIdIn")
    public List<TaiNguyenDTO> findByIdIn(@RequestParam(value = "taiNguyenIds", defaultValue = "") String idsStr) {

        if (idsStr.length() > 0) {
            List<Long> ids = StringUtils.convertStringToLongs(idsStr);

            return taiNguyenService.findByTaiNguyenIn(ids);
        }

        return new ArrayList<>();
    }

    @GetMapping("/findAllTaiNguyenByCoQuanIdAndChucVuId")
    public String findAllTaiNguyenByCoQuanIdAndChucVuId(@RequestParam(value = "coQuanId", defaultValue = "") long coQuanId,
                                                        @RequestParam(value = "chucVuId", defaultValue = "") long chucVuId) {
        return taiNguyenService.findAllTaiNguyenByCoQuanIdAndChucVuId(coQuanId, chucVuId);
    }

}
