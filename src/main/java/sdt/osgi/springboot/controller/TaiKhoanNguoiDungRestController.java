package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.TaiKhoanNguoiDungDTO;
import sdt.osgi.springboot.model.TaiKhoanNguoiDung;
import sdt.osgi.springboot.service.TaiKhoanNguoiDungService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/taikhoannguoidung")
public class TaiKhoanNguoiDungRestController {

	private final TaiKhoanNguoiDungService taiKhoanNguoiDungService;

	@GetMapping("/{taiKhoanNguoiDungId}")
	public TaiKhoanNguoiDungDTO getTaiKhoanNguoiDung(@PathVariable Long taiKhoanNguoiDungId) {

		return taiKhoanNguoiDungService.getTaiKhoanNguoiDung(taiKhoanNguoiDungId);
	}

	@GetMapping("/findByUserId/{userId}")
	public TaiKhoanNguoiDungDTO getTaiKhoanNguoiDungByUserId(@PathVariable Long userId) {

		return taiKhoanNguoiDungService.findByUserId(userId);
	}

	@GetMapping
	public Page<TaiKhoanNguoiDung> getTaiKhoanNguoiDungs(@RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
														 @RequestParam(value = "size", defaultValue = "20") int size){

		return taiKhoanNguoiDungService.findAll(pageIndex,size);
	}

	@GetMapping("/findTop1000")
	public List<TaiKhoanNguoiDung> findTop1000(){

		return taiKhoanNguoiDungService.findTop1000();
	}

	@PutMapping("/update")
	public TaiKhoanNguoiDungDTO updateTaiKhoanNguoiDung(@RequestBody TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO) {

		taiKhoanNguoiDungService.update(taiKhoanNguoiDungDTO);

		return taiKhoanNguoiDungDTO;
	}

	@PostMapping("/create")
	public TaiKhoanNguoiDungDTO addTaiKhoanNguoiDung(@RequestBody TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO) {

		taiKhoanNguoiDungService.add(taiKhoanNguoiDungDTO);

		return taiKhoanNguoiDungDTO;
	}

	@DeleteMapping("/delete/{taiKhoanNguoiDungId}")
	public boolean deleteTaiKhoanNguoiDung(@PathVariable Long taiKhoanNguoiDungId) {

		taiKhoanNguoiDungService.delete(taiKhoanNguoiDungId);

		return true;
	}

	@GetMapping("/findByEmail")
	public TaiKhoanNguoiDungDTO getTaiKhoanNguoiDung(@RequestParam(value = "email", defaultValue = "") String email) {

		return taiKhoanNguoiDungService.findFirstByEmail(email);
	}

	@GetMapping("/findRoleByEmail")
	public List<Long> getRolesByEmail(@RequestParam(value = "email", defaultValue = "") String email) {

		return taiKhoanNguoiDungService.getListRoleIds(email);
	}
}