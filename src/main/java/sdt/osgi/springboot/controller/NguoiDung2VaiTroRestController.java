package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.NguoiDung2VaiTroDTO;
import sdt.osgi.springboot.model.NguoiDung2VaiTro;
import sdt.osgi.springboot.service.NguoiDung2VaiTroService;
import sdt.osgi.springboot.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/nguoidung2vaitro")
public class NguoiDung2VaiTroRestController {

    private final NguoiDung2VaiTroService nguoiDung2VaiTroService;

    @GetMapping("/{nguoiDung2VaiTroId}")
    public NguoiDung2VaiTroDTO getNguoiDung2VaiTro(@PathVariable Long nguoiDung2VaiTroId) {

        return nguoiDung2VaiTroService.getNguoiDung2VaiTro(nguoiDung2VaiTroId);
    }

    @GetMapping("/listAllNguoiDung2VaiTro")
    public List<NguoiDung2VaiTroDTO> getListNguoiDung2VaiTro() {

        return nguoiDung2VaiTroService.getListNguoiDung2VaiTro();
    }

    @GetMapping("/findTop1000")
    public List<NguoiDung2VaiTro> findTop1000() {

        return nguoiDung2VaiTroService.findTop1000();
    }

    @PutMapping("/update")
    public NguoiDung2VaiTroDTO updateNguoiDung2VaiTro(@RequestBody NguoiDung2VaiTroDTO nguoiDung2VaiTroDTO) {

        nguoiDung2VaiTroService.update(nguoiDung2VaiTroDTO);

        return nguoiDung2VaiTroDTO;
    }

    @PostMapping("/create")
    public NguoiDung2VaiTroDTO addNguoiDung2VaiTro(@RequestBody NguoiDung2VaiTroDTO nguoiDung2VaiTroDTO) {

        nguoiDung2VaiTroService.add(nguoiDung2VaiTroDTO);

        return nguoiDung2VaiTroDTO;
    }

    @DeleteMapping("/delete/{nguoiDung2VaiTroId}")
    public boolean deleteNguoiDung2VaiTro(@PathVariable Long nguoiDung2VaiTroId) {

        nguoiDung2VaiTroService.delete(nguoiDung2VaiTroId);

        return true;

    }

    @DeleteMapping("/deleteByCongChucAndVaiTro/{congChucId}/{vaiTroId}")
    public boolean deleteByCoQuanAndChucVu(@PathVariable(value = "congChucId") long congChucId,
                                           @PathVariable(value = "vaiTroId") long vaiTroId) {
        nguoiDung2VaiTroService.deleteByCongChucIdAndVaiTroId(congChucId, vaiTroId);
        return true;
    }

    @GetMapping("/findByIdIn")
    public List<NguoiDung2VaiTroDTO> findByIdIn(@RequestParam(value = "nguoiDung2VaiTroIds", defaultValue = "") String idsStr) {

        if (idsStr.length() > 0) {
            List<Long> ids = StringUtils.convertStringToLongs(idsStr);

            return nguoiDung2VaiTroService.findByNguoiDung2VaiTroIn(ids);
        }

        return new ArrayList<>();
    }


    @GetMapping("/findAllByCongChucId")
    public List<NguoiDung2VaiTroDTO> findAllByCongChucId(@RequestParam(value = "congChucId", defaultValue = "") long congChucId) {
        return nguoiDung2VaiTroService.findAllByCongChucId(congChucId);
    }

    @PutMapping("/updateListNguoiDung2VaiTro")
    public List<NguoiDung2VaiTroDTO> updateListNguoiDung2VaiTro(@RequestBody List<NguoiDung2VaiTroDTO> nguoiDung2VaiTroDTOS) {

        return nguoiDung2VaiTroService.saveListNguoiDung2VaiTroDTO(nguoiDung2VaiTroDTOS);
    }
}
