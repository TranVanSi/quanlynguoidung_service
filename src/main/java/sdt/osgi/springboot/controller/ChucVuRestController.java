package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.ChucVuDTO;
import sdt.osgi.springboot.mapper.ChucVuMapper;
import sdt.osgi.springboot.model.ChucVu;
import sdt.osgi.springboot.service.ChucVuService;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.util.StringUtils;

import javax.validation.Valid;
import java.net.URLDecoder;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/chucvu")
public class ChucVuRestController {

	private final ChucVuService chucVuService;
	private final ChucVuMapper chucVuMapper;

	@GetMapping("/{chucVuId}")
	public ChucVuDTO getChucVu(@PathVariable Long chucVuId) {

		return chucVuService.getChucVu(chucVuId);
	}

	@GetMapping("/listAllChucVu")
	public List<ChucVuDTO> getListAllChucVu() throws Exception {

		return chucVuService.getListChucVu();
	}

	@GetMapping("/listAllChucVuBySearch")
	public List<ChucVuDTO> getListAllChucVuBySearch(@RequestParam(value = "keyword", defaultValue = "") String keyword) throws Exception {
		if (keyword.trim().length() > 0) {
			keyword = URLDecoder.decode(keyword, "UTF-8");
		}
		return chucVuService.getListChucVuBySearch(keyword);
	}

	@PostMapping
	public DataTablesOutput<ChucVu> getChucVus(@Valid @RequestBody DataTablesInput input){

		return chucVuService.findAll(input);
	}

	@GetMapping
	public Page<ChucVu> getChucVus(@RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
									@RequestParam(value = "size", defaultValue = "20") int size,
								   @RequestParam(value = "keyword", defaultValue = "") String keyword) throws Exception{

		if (keyword.trim().length() > 0) {
            keyword = URLDecoder.decode(keyword, "UTF-8");
			return chucVuService.findByKeyword(keyword, pageIndex, size);
		}

		return chucVuService.findAll(pageIndex,size);
	}

	@GetMapping("/findTop1000")
	public List<ChucVu> findTop1000(){

		return chucVuService.findTop1000();
	}

	@GetMapping("/findTop1000ByIdNotIn")
	public List<ChucVuDTO> findTop1000(@RequestParam(value = "chucVuIds", defaultValue = "") String idsStr){

		if (idsStr.length() > 0) {
			List<Long> ids = StringUtils.convertStringToLongs(idsStr);

			return chucVuService.findTop1000ByIdNotIn(ids);
		}

		return chucVuMapper.chucVusToChucVuDTOs(chucVuService.findTop1000());
	}

	@GetMapping("/findByMa")
	public ChucVuDTO findFirstByMa(@RequestParam(value = "ma", defaultValue = "") String ma){

		return chucVuService.findFirstByMa(ma);
	}

	@GetMapping("/findByTen")
	public ChucVuDTO findFirstByTen(@RequestParam(value = "ten", defaultValue = "") String ten) throws Exception {

		return chucVuService.findFirstByTen(StringUtils.decode(ten));
	}

	@PutMapping("/update")
	public ChucVuDTO updateChucVu(@RequestBody ChucVuDTO chucVuDTO) {

		chucVuService.update(chucVuDTO);

		return chucVuDTO;
	}

    @PostMapping("/create")
    public ChucVuDTO addChucVu(@RequestBody ChucVuDTO chucVuDTO) {

        chucVuService.add(chucVuDTO);

        return chucVuDTO;
    }

	@DeleteMapping("/delete/{chucVuId}")
	public boolean deleteChucVu(@PathVariable Long chucVuId) {

		chucVuService.delete(chucVuId);

		return true;

	}
}
