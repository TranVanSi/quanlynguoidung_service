package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import sdt.osgi.springboot.service.UploadFileService;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/upload")
public class UploadFileRestController {
    private final UploadFileService uploadFileService;

    @RequestMapping(value = "/chucvu", method = RequestMethod.POST)
    public void uploadExcelFileChucVu(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        uploadFileService.saveAllChucVu(file, nguoiTao);
    }
}
