package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sdt.osgi.springboot.dto.DonViHanhChinhDTO;
import sdt.osgi.springboot.service.DonViHanhChinhService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/dvhc")
@RequiredArgsConstructor
public class DonViHanhChinhRestController {
    private final DonViHanhChinhService donViHanhChinhService;

    @GetMapping(value = "/capCoQuanQuanLyId/{capId}", produces = MediaType.APPLICATION_JSON_VALUE)
    private List<DonViHanhChinhDTO> findByCapCoQuanId(@PathVariable long capId) {
        return donViHanhChinhService.findByCapCoQuanId(capId);
    }
}

