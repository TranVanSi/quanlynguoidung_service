package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import sdt.osgi.springboot.model.Record;
import sdt.osgi.springboot.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class SimpleController {

    private final RecordService recordService;

    @GetMapping("/echo")
    public Iterable<Record> echo() {
        return recordService.getRecords();
    }

}
