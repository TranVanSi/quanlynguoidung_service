package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.osgi.springboot.dto.ChucVuDTO;
import sdt.osgi.springboot.model.ChucVu2CoQuan2VaiTro;
import sdt.osgi.springboot.model.VaiTro;
import sdt.osgi.springboot.service.ChucVu2CoQuan2VaiTroService;
import sdt.osgi.springboot.util.StringUtils;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/chucvu2coquan2vaitro")
public class ChucVu2CoQuan2VaiTroRestController {

    private final ChucVu2CoQuan2VaiTroService chucVu2CoQuan2VaiTroService;

    @GetMapping("/findByCoQuanQuanLyId")
    public List<ChucVu2CoQuan2VaiTroDTO> findByCoQuanQuanLyId(@RequestParam(value = "coQuanQuanLyId") long coQuanQuanLyId) {

        return chucVu2CoQuan2VaiTroService.findByCoQuanQuanLyId(coQuanQuanLyId);
    }

    @GetMapping("/findByCoQuanQuanLyIdAndSearchChucVu")
    public List<ChucVu2CoQuan2VaiTroDTO> findByCoQuanQuanLyIdAndSearchChucVu(@RequestParam(value = "coQuanQuanLyId") long coQuanQuanLyId,
                                                                             @RequestParam(value = "txtSearch") String txtSearch) throws Exception {
        return chucVu2CoQuan2VaiTroService.findByCoQuanQuanLyIdAndSearchChucVu(coQuanQuanLyId, StringUtils.decode(txtSearch));
    }

    @PostMapping("/create")
    public ChucVu2CoQuan2VaiTroDTO addChucVu2CoQuan2VaiTro(@RequestBody ChucVu2CoQuan2VaiTroDTO chucVu2CoQuan2VaiTroDTO) {

        chucVu2CoQuan2VaiTroService.add(chucVu2CoQuan2VaiTroDTO);

        return chucVu2CoQuan2VaiTroDTO;
    }

    @DeleteMapping("/delete/{chucVu2CoQuan2VaiTroId}")
    public boolean deleteChucVu2CoQuan2VaiTro(@PathVariable Long chucVu2CoQuan2VaiTroId) {

        chucVu2CoQuan2VaiTroService.delete(chucVu2CoQuan2VaiTroId);

        return true;

    }

    @PostMapping("/findByCoQuanId/{coQuanQuanLyId}")
    public DataTablesOutput<ChucVu2CoQuan2VaiTro> findByCoQuanQuanLyId(@Valid @RequestBody DataTablesInput input,
                                                                       @PathVariable(value = "coQuanQuanLyId") long coQuanId) throws Exception {

        return chucVu2CoQuan2VaiTroService.getChucVu2CoQuan2VaiTros(coQuanId,
                input.getSearch().getValue() != null ? input.getSearch().getValue() : "", input.getStart(), input.getLength(), input.getDraw());

    }

    @GetMapping("/findByCoQuanAndChucVu/{coQuanQuanLyId}/{chucVuId}")
    public List<ChucVu2CoQuan2VaiTro> findByCoQuanQuanLyId(@PathVariable(value = "coQuanQuanLyId") long coQuanId,
                                                           @PathVariable(value = "chucVuId") long chucVuId) {

        return chucVu2CoQuan2VaiTroService.findByCoQuanAndChucVu(coQuanId, chucVuId);

    }

    @DeleteMapping("/deleteByCoQuanAndChucVu/{coQuanQuanLyId}/{chucVuId}")
    public boolean deleteByCoQuanAndChucVu(@PathVariable(value = "coQuanQuanLyId") long coQuanId,
                                           @PathVariable(value = "chucVuId") long chucVuId) {

        chucVu2CoQuan2VaiTroService.deleteChucVu2VaiTroByCoQuan(coQuanId, chucVuId);

        return true;

    }

    @PostMapping("/createByCoQuanAndChucVu/{coQuanQuanLyId}/{chucVuId}")
    public boolean createByCoQuanAndChucVu(@RequestBody List<VaiTro> vaiTros,
                                           @PathVariable(value = "coQuanQuanLyId") long coQuanId,
                                           @PathVariable(value = "chucVuId") long chucVuId) {

        chucVu2CoQuan2VaiTroService.createChucVu2VaiTroByCoQuan(coQuanId, chucVuId, vaiTros);

        return true;

    }

    @GetMapping("/findChucVuByCoQuanId/{coQuanQuanLyId}")
    public List<Long> findChucVuByCoQuanId(@PathVariable(value = "coQuanQuanLyId") long coQuanId) {

        return chucVu2CoQuan2VaiTroService.findChucVuByCoQuanId(coQuanId);

    }

    @GetMapping("/findChucVuByCoQuanQuanLyId/{coQuanQuanLyId}")
    public List<ChucVuDTO> findChucVuByCoQuanQuanLyId(@PathVariable(value = "coQuanQuanLyId") long coQuanId) {

        return chucVu2CoQuan2VaiTroService.findChucVuByCoQuanQuanLyId(coQuanId);

    }

    @GetMapping("/findRoleIdsByMa")
    public List<Long> findRoleIdsByMaCoQuanAndMaChucVu(@RequestParam(value = "maChucVu", defaultValue = "") String maChucVu,
                                                       @RequestParam(value = "maCoQuan", defaultValue = "") String maCoQuan) {

        return chucVu2CoQuan2VaiTroService.findRoleIdsByMaCoQuanAndMaChucVu(maCoQuan, maChucVu);
    }
}
