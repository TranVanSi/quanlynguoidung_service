package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dto.ChucVuDTO;

@Service
@RequiredArgsConstructor
public class UploadValidate {

    public boolean validateChucVu(ChucVuDTO itemDraft, ChucVuDTO itemByMa) throws Exception {

        boolean valid = true;
        long chucVuId = itemDraft.getId();

        if ((chucVuId == 0 && itemByMa != null && itemDraft.getMa().equals(itemByMa.getMa())) ||
                (chucVuId > 0 && itemByMa != null && chucVuId != itemByMa.getId())) {
            valid = false;
        }

        return valid;
    }

}
