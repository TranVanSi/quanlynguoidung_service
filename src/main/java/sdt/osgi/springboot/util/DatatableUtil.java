package sdt.osgi.springboot.util;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import java.util.List;

public class DatatableUtil {

    public static DataTablesOutput convertPageToDataTable (Page objects, int draw) {
        DataTablesOutput<Object> dataTablesOutput = new DataTablesOutput();

        dataTablesOutput.setData((List<Object>) objects.getContent());
        dataTablesOutput.setDraw(draw + 1);
        dataTablesOutput.setError(dataTablesOutput.getError());
        dataTablesOutput.setRecordsTotal(objects.getTotalElements());
        dataTablesOutput.setRecordsFiltered(objects.getTotalElements());

        return dataTablesOutput;
    }
}
