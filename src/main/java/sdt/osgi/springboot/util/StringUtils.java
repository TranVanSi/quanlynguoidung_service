package sdt.osgi.springboot.util;

import java.math.BigInteger;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringUtils {
    public static List<Long> convertStringToLongs (String idsStr) {

        return Stream.of(idsStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
    }

    public static List<Long> convertListBigIntToListLong (List<BigInteger> bigIntegers) {

       return bigIntegers.stream().mapToLong(BigInteger::longValue).boxed().collect(Collectors.toList());
    }

    public static String decode (String chuoi) throws Exception{
        return URLDecoder.decode(chuoi, "UTF-8");
    }

    public static Date convertStringToDate (String dateStr) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if (dateStr.length() > 0) {
            return sdf.parse(dateStr);
        }

        return null;
    }
}
