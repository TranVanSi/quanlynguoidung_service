package sdt.osgi.springboot.util;

public class Constants {
    public static final int DA_XOA = 1;
    public static final int TAO_MOI = 0;
    public static final String KHONG_TIM_THAY = "Không tìm thấy";
    public interface TrangThaiHoatDong {
        public final int KHOA = 1;
        public final int MO_KHOA = 0;
    }
}
